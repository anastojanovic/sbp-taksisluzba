﻿using SBP_faza_2.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ValidatorPodataka;

namespace SBP_faza_2.Forme
{
    public partial class MusterijaOperationForm : SBP_faza_2.Forme.AbstractOperationForm
    {
        public MusterijaOperationForm()
        {
            InitializeComponent();
        }

        public override void KontroleUObjekat()
        {
            try
            {

                Validator.ValidirajAdresu(txtAdresa.Text);
                base.Dto = new MusterijaBasicDTO(0, txtAdresa.Text);          
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }

        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto != null)
            {
                MusterijaLongDTO a = (MusterijaLongDTO)dto;
                txtAdresa.Text = a.Adresa;
                lblVoznja.Text = a.BrojVoznji.ToString();
            }
        }

    }
}
