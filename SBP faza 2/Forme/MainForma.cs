﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using SBP_faza_2.Entiteti;
using SBP_faza_2.DTOManagers;
namespace SBP_faza_2.Forme
{
    public partial class MainForma: Form
    {
        public MainForma()
        {
            InitializeComponent();
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            ListForm lf = new ListForm(new AdministratorDTOManager(), new AdminOperationForm());
            lf.Show();

        }

        private void btnVozac_Click(object sender, EventArgs e)
        {
            ListForm lf = new VozacListForm();
            lf.Show();
        }

        private void btnVozilaFirme_Click(object sender, EventArgs e)
        {
            ListForm lf = new ListForm(new VoziloFirmeDTOManager(), new VoziloFirmeOperationForm());
            lf.Show();
        }

        private void btnPrikaziMusterije_Click(object sender, EventArgs e)
        {
            ListForm lf = new MusterijaListForm();
            lf.Show();
        }

        private void btnPrikaziVoznje_Click(object sender, EventArgs e)
        {
            ListForm lf = new ListForm(new VoznjaDTOManager(), new VoznjaOperationForm());
            lf.Show();
        }
    }
}
