﻿namespace SBP_faza_2.Forme
{
    partial class MusterijaListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTelefoni = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTelefoni
            // 
            this.btnTelefoni.Location = new System.Drawing.Point(20, 325);
            this.btnTelefoni.Name = "btnTelefoni";
            this.btnTelefoni.Size = new System.Drawing.Size(113, 23);
            this.btnTelefoni.TabIndex = 9;
            this.btnTelefoni.Text = "Telefoni";
            this.btnTelefoni.UseVisualStyleBackColor = true;
            this.btnTelefoni.Click += new System.EventHandler(this.btnTelefoni_Click);
            // 
            // MusterijaListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(793, 371);
            this.Controls.Add(this.btnTelefoni);
            this.Name = "MusterijaListForm";
            this.Controls.SetChildIndex(this.btnTelefoni, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTelefoni;
    }
}
