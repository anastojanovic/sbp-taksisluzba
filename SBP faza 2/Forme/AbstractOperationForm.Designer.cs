﻿namespace SBP_faza_2.Forme
{
    partial class AbstractOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOdustani = new System.Windows.Forms.Button();
            this.btnZapamti = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOdustani
            // 
            this.btnOdustani.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOdustani.Location = new System.Drawing.Point(184, 440);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(88, 43);
            this.btnOdustani.TabIndex = 33;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseVisualStyleBackColor = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // btnZapamti
            // 
            this.btnZapamti.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnZapamti.Location = new System.Drawing.Point(76, 440);
            this.btnZapamti.Name = "btnZapamti";
            this.btnZapamti.Size = new System.Drawing.Size(88, 43);
            this.btnZapamti.TabIndex = 32;
            this.btnZapamti.Text = "Zapamti";
            this.btnZapamti.UseVisualStyleBackColor = true;
            this.btnZapamti.Click += new System.EventHandler(this.btnZapamti_Click);
            // 
            // AbstractOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 524);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.btnZapamti);
            this.Name = "AbstractOperationForm";
            this.Text = "Dodavanje/Azuriranje";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AbstractOperationForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AbstractOperationForm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOdustani;
        private System.Windows.Forms.Button btnZapamti;
    }
}