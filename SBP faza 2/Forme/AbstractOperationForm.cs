﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SBP_faza_2.DTOs;

namespace SBP_faza_2.Forme
{
    public partial class AbstractOperationForm : Form
    {
        public AbstractDTO Dto { get; set; }

        public virtual void KontroleUObjekat() { throw new NotImplementedException(); }
        public virtual void ObjekatUKontrole(AbstractDTO dto) { throw new NotImplementedException(); }

        public AbstractOperationForm()
        {           
          InitializeComponent();
        }


        private void btnZapamti_Click(object sender, EventArgs e)
        {
            KontroleUObjekat();
            if (Dto != null)
            {
               this.DialogResult = DialogResult.OK;

            }
            else
            {
                this.DialogResult = DialogResult.None;
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da zelite da zatvorite formu?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
               this.DialogResult = DialogResult.Cancel;

            }
            else
            {
                this.DialogResult = DialogResult.None;
            }

        }

        private void ResetujFormu()
        {
            foreach (TextBox tb in this.Controls.OfType<TextBox>().ToArray())
            {
                tb.Clear();
            }

            foreach(DateTimePicker dtp in this.Controls.OfType<DateTimePicker>().ToArray())
            {
                dtp.Value = DateTime.Today;
            }
        }

        private void AbstractOperationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ResetujFormu();
        }

        private void AbstractOperationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult == DialogResult.None)
            {
                e.Cancel = true;
                return;
            }
        }
    }
}
