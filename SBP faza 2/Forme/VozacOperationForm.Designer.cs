﻿namespace SBP_faza_2.Forme
{
    partial class VozacOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSrednjeSlovo = new System.Windows.Forms.TextBox();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtJMBG = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtBrojDozvole = new System.Windows.Forms.TextBox();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.lblJMBG = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblBrojDozvole = new System.Windows.Forms.Label();
            this.btnStrucnaSprema = new System.Windows.Forms.Label();
            this.lblSrednjeSlovo = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.lblKategorija = new System.Windows.Forms.Label();
            this.txtKategorija = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtSrednjeSlovo
            // 
            this.txtSrednjeSlovo.Location = new System.Drawing.Point(190, 78);
            this.txtSrednjeSlovo.Name = "txtSrednjeSlovo";
            this.txtSrednjeSlovo.Size = new System.Drawing.Size(156, 22);
            this.txtSrednjeSlovo.TabIndex = 47;
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(190, 114);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(156, 22);
            this.txtPrezime.TabIndex = 46;
            // 
            // txtJMBG
            // 
            this.txtJMBG.Location = new System.Drawing.Point(190, 151);
            this.txtJMBG.Name = "txtJMBG";
            this.txtJMBG.Size = new System.Drawing.Size(156, 22);
            this.txtJMBG.TabIndex = 45;
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(190, 189);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(156, 22);
            this.txtAdresa.TabIndex = 44;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(190, 229);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(156, 22);
            this.txtTelefon.TabIndex = 43;
            // 
            // txtBrojDozvole
            // 
            this.txtBrojDozvole.Location = new System.Drawing.Point(190, 267);
            this.txtBrojDozvole.Name = "txtBrojDozvole";
            this.txtBrojDozvole.Size = new System.Drawing.Size(156, 22);
            this.txtBrojDozvole.TabIndex = 42;
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(190, 39);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(156, 22);
            this.txtIme.TabIndex = 41;
            // 
            // lblJMBG
            // 
            this.lblJMBG.AutoSize = true;
            this.lblJMBG.Location = new System.Drawing.Point(33, 151);
            this.lblJMBG.Name = "lblJMBG";
            this.lblJMBG.Size = new System.Drawing.Size(50, 17);
            this.lblJMBG.TabIndex = 40;
            this.lblJMBG.Text = "JMBG:";
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Location = new System.Drawing.Point(33, 229);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(60, 17);
            this.lblTelefon.TabIndex = 39;
            this.lblTelefon.Text = "Telefon:";
            // 
            // lblBrojDozvole
            // 
            this.lblBrojDozvole.AutoSize = true;
            this.lblBrojDozvole.Location = new System.Drawing.Point(33, 267);
            this.lblBrojDozvole.Name = "lblBrojDozvole";
            this.lblBrojDozvole.Size = new System.Drawing.Size(146, 17);
            this.lblBrojDozvole.TabIndex = 38;
            this.lblBrojDozvole.Text = "Broj vozačke dozvole:";
            // 
            // btnStrucnaSprema
            // 
            this.btnStrucnaSprema.AutoSize = true;
            this.btnStrucnaSprema.Location = new System.Drawing.Point(33, 189);
            this.btnStrucnaSprema.Name = "btnStrucnaSprema";
            this.btnStrucnaSprema.Size = new System.Drawing.Size(57, 17);
            this.btnStrucnaSprema.TabIndex = 37;
            this.btnStrucnaSprema.Text = "Adresa:";
            // 
            // lblSrednjeSlovo
            // 
            this.lblSrednjeSlovo.AutoSize = true;
            this.lblSrednjeSlovo.Location = new System.Drawing.Point(33, 78);
            this.lblSrednjeSlovo.Name = "lblSrednjeSlovo";
            this.lblSrednjeSlovo.Size = new System.Drawing.Size(102, 17);
            this.lblSrednjeSlovo.TabIndex = 36;
            this.lblSrednjeSlovo.Text = "Srednje slovo: ";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(33, 114);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(63, 17);
            this.lblPrezime.TabIndex = 35;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(33, 39);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(38, 17);
            this.lblIme.TabIndex = 34;
            this.lblIme.Text = "Ime: ";
            // 
            // lblKategorija
            // 
            this.lblKategorija.AutoSize = true;
            this.lblKategorija.Location = new System.Drawing.Point(33, 307);
            this.lblKategorija.Name = "lblKategorija";
            this.lblKategorija.Size = new System.Drawing.Size(80, 17);
            this.lblKategorija.TabIndex = 48;
            this.lblKategorija.Text = "Kategorija: ";
            // 
            // txtKategorija
            // 
            this.txtKategorija.Location = new System.Drawing.Point(190, 307);
            this.txtKategorija.Name = "txtKategorija";
            this.txtKategorija.Size = new System.Drawing.Size(156, 22);
            this.txtKategorija.TabIndex = 49;
            // 
            // VozacOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(375, 532);
            this.Controls.Add(this.txtKategorija);
            this.Controls.Add(this.lblKategorija);
            this.Controls.Add(this.txtSrednjeSlovo);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtJMBG);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.txtBrojDozvole);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.lblJMBG);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.lblBrojDozvole);
            this.Controls.Add(this.btnStrucnaSprema);
            this.Controls.Add(this.lblSrednjeSlovo);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.Name = "VozacOperationForm";
            this.Controls.SetChildIndex(this.lblIme, 0);
            this.Controls.SetChildIndex(this.lblPrezime, 0);
            this.Controls.SetChildIndex(this.lblSrednjeSlovo, 0);
            this.Controls.SetChildIndex(this.btnStrucnaSprema, 0);
            this.Controls.SetChildIndex(this.lblBrojDozvole, 0);
            this.Controls.SetChildIndex(this.lblTelefon, 0);
            this.Controls.SetChildIndex(this.lblJMBG, 0);
            this.Controls.SetChildIndex(this.txtIme, 0);
            this.Controls.SetChildIndex(this.txtBrojDozvole, 0);
            this.Controls.SetChildIndex(this.txtTelefon, 0);
            this.Controls.SetChildIndex(this.txtAdresa, 0);
            this.Controls.SetChildIndex(this.txtJMBG, 0);
            this.Controls.SetChildIndex(this.txtPrezime, 0);
            this.Controls.SetChildIndex(this.txtSrednjeSlovo, 0);
            this.Controls.SetChildIndex(this.lblKategorija, 0);
            this.Controls.SetChildIndex(this.txtKategorija, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSrednjeSlovo;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtJMBG;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.TextBox txtBrojDozvole;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label lblJMBG;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblBrojDozvole;
        private System.Windows.Forms.Label btnStrucnaSprema;
        private System.Windows.Forms.Label lblSrednjeSlovo;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblKategorija;
        private System.Windows.Forms.TextBox txtKategorija;
    }
}
