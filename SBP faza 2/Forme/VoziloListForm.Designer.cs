﻿namespace SBP_faza_2.Forme
{
    partial class VoziloListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajVozila = new System.Windows.Forms.Button();
            this.lblVozac = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDodajVozila
            // 
            this.btnDodajVozila.Location = new System.Drawing.Point(20, 319);
            this.btnDodajVozila.Name = "btnDodajVozila";
            this.btnDodajVozila.Size = new System.Drawing.Size(393, 23);
            this.btnDodajVozila.TabIndex = 9;
            this.btnDodajVozila.Text = "Dodaj postojeće vozilo u vlasništvo vozača";
            this.btnDodajVozila.UseVisualStyleBackColor = true;
            this.btnDodajVozila.Click += new System.EventHandler(this.btnDodajVozila_Click);
            // 
            // lblVozac
            // 
            this.lblVozac.AutoSize = true;
            this.lblVozac.Location = new System.Drawing.Point(434, 235);
            this.lblVozac.Name = "lblVozac";
            this.lblVozac.Size = new System.Drawing.Size(55, 17);
            this.lblVozac.TabIndex = 10;
            this.lblVozac.Text = "Vozač :";
            // 
            // VoziloListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(702, 355);
            this.Controls.Add(this.lblVozac);
            this.Controls.Add(this.btnDodajVozila);
            this.Name = "VoziloListForm";
            this.Text = "Vozila";
            this.Controls.SetChildIndex(this.btnDodajVozila, 0);
            this.Controls.SetChildIndex(this.lblVozac, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodajVozila;
        private System.Windows.Forms.Label lblVozac;
    }
}
