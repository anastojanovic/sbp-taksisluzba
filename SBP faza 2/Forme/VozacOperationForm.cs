﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ValidatorPodataka;
using SBP_faza_2.DTOs;
namespace SBP_faza_2.Forme
{
    public partial class VozacOperationForm : AbstractOperationForm
    {
        public VozacOperationForm()
        {
            InitializeComponent();
        }

        public override void KontroleUObjekat()
        {
            try
            {
                Validator.ValidirajIme(txtIme.Text);
                Validator.ValidirajSlovo(txtSrednjeSlovo.Text);
                Validator.ValidirajIme(txtPrezime.Text);
                Validator.ValidirajGenericBroj(txtJMBG.Text, "Jmbg");
                Validator.ValidirajAdresu(txtAdresa.Text);
                Validator.ValidirajTelefon(txtTelefon.Text);
                Validator.ValidirajKategoriju(txtKategorija.Text);
                Validator.ValidirajGenericBroj(txtBrojDozvole.Text, "Broj dozvole");
                base.Dto = new VozacLongDTO()
                {
                    LicnoIme = txtIme.Text,
                    SrednjeSlovo = txtSrednjeSlovo.Text,
                    Prezime = txtPrezime.Text,
                    JMBG = Int64.Parse(txtJMBG.Text),
                    Adresa = txtAdresa.Text,
                    Telefon = Int32.Parse(txtTelefon.Text),
                    Kategorija = txtKategorija.Text,
                    BrojDozvole = Int32.Parse(txtBrojDozvole.Text)
                };

            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }

        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto != null)
            {
                VozacLongDTO v = (VozacLongDTO)dto;
                txtIme.Text = v.LicnoIme;
                txtSrednjeSlovo.Text = v.SrednjeSlovo;
                txtPrezime.Text = v.Prezime;
                txtJMBG.Text = v.JMBG.ToString();
                txtAdresa.Text = v.Adresa;
                txtTelefon.Text = v.Telefon.ToString();
                txtKategorija.Text = v.Kategorija;
                txtBrojDozvole.Text = v.BrojDozvole.ToString();
            }
        }
    }
}
