﻿namespace SBP_faza_2.Forme
{
    partial class TelefonMusterijeListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMusterija = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblMusterija
            // 
            this.lblMusterija.AutoSize = true;
            this.lblMusterija.Location = new System.Drawing.Point(434, 235);
            this.lblMusterija.Name = "lblMusterija";
            this.lblMusterija.Size = new System.Drawing.Size(69, 17);
            this.lblMusterija.TabIndex = 9;
            this.lblMusterija.Text = "Musterija:";
            // 
            // TelefonMusterijeListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(793, 333);
            this.Controls.Add(this.lblMusterija);
            this.Name = "TelefonMusterijeListForm";
            this.Controls.SetChildIndex(this.lblMusterija, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMusterija;
    }
}
