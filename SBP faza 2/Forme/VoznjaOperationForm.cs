﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using SBP_faza_2.DTOManagers;

namespace SBP_faza_2.Forme
{
    public partial class VoznjaOperationForm : SBP_faza_2.Forme.AbstractOperationForm
    {
        MusterijaDTOManager mdm;
        AdministratorDTOManager adm;
        VozacDTOManager vdm;

        public VoznjaOperationForm()
        {
        
            InitializeComponent();
            adm = new AdministratorDTOManager();
            mdm = new MusterijaDTOManager();
            vdm = new VozacDTOManager();

          //treba da se doda null vrednost za admina i musteriju
            cbxAdmini.DataSource = adm.ReadAll();
            cbxMusterije.DataSource = mdm.ReadAll();
            cbxVozaci.DataSource = vdm.ReadAll();
           

        }

        public override void KontroleUObjekat()
        {
            try
            {
                ValidatorPodataka.Validator.ValidirajAdresu(txtPocetnaStanica.Text);
                ValidatorPodataka.Validator.ValidirajAdresu(txtKrajnjaStanica.Text);
                ValidatorPodataka.Validator.ValidirajTelefon(txtTelefon.Text);
                ValidatorPodataka.Validator.ValidirajDatum(dtpVremePocetka.Value, dtpVremeKraja.Value);
                ValidatorPodataka.Validator.ValidirajDatum(dtpVremePrimanjaPoziva.Value, dtpVremePocetka.Value);
                VoznjaLongDTO voznja = new VoznjaLongDTO(0, txtPocetnaStanica.Text, txtKrajnjaStanica.Text, dtpVremePocetka.Value, dtpVremeKraja.Value, dtpVremePrimanjaPoziva.Value, Int32.Parse(txtTelefon.Text));
                //base.Dto = new VoznjaLongDTO(0, txtPocetnaStanica.Text, txtKrajnjaStanica.Text, dtpVremePocetka.Value, dtpVremeKraja.Value, dtpVremePrimanjaPoziva.Value, Int32.Parse(txtTelefon.Text));
                MusterijaBasicDTO m = (MusterijaBasicDTO)cbxMusterije.SelectedItem;
                
                AdministratorBasicDTO a = (AdministratorBasicDTO)cbxAdmini.SelectedItem;
                
                VozacBasicDTO v = (VozacBasicDTO)cbxVozaci.SelectedItem;
                
                base.Dto = new VoznjaLongDTO(0, txtPocetnaStanica.Text, txtKrajnjaStanica.Text, dtpVremePocetka.Value, dtpVremeKraja.Value, dtpVremePrimanjaPoziva.Value, Int32.Parse(txtTelefon.Text))
                {
                    IdMusterije=(int)m.Id,
                    IdAdministratora= (int)a.Id,
                    IdVozaca= (int)v.Id
                };

            }
            catch(ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }
        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if(dto != null)
            {
                try
                {
                    VoznjaLongDTO v = (VoznjaLongDTO)dto;
                    ValidatorPodataka.Validator.ValidirajDatum(v.VremePocetka);
                    ValidatorPodataka.Validator.ValidirajDatum(v.VremeKraja);
                    ValidatorPodataka.Validator.ValidirajDatum(v.VremePrimanjaPoziva);

                    txtPocetnaStanica.Text = v.PocetnaStanica;
                    txtKrajnjaStanica.Text = v.KrajnjaStanica;
                    dtpVremePocetka.Value = v.VremePocetka;
                    dtpVremeKraja.Value = v.VremeKraja;
                    dtpVremePrimanjaPoziva.Value = v.VremePrimanjaPoziva;
                    txtTelefon.Text = v.PozivniTelefon.ToString();

                    /*  cbxAdmini.SelectedItem = adm.Read(v.IdAdministratora);
                      cbxMusterije.SelectedItem = mdm.Read(v.IdMusterije);
                      cbxVozaci.SelectedItem = vdm.Read(v.IdVozaca);
                      */
                    cbxAdmini.Text = adm.ReadShort(v.IdAdministratora).ToString();
                    cbxMusterije.Text = mdm.ReadShort(v.IdMusterije).ToString();
                    cbxVozaci.Text = vdm.ReadShort(v.IdVozaca).ToString();
                }
                catch(ArgumentException e)
                {
                    MessageBox.Show(e.Message);
                }
             
            }
        }
    }
}
