﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using ValidatorPodataka;
namespace SBP_faza_2.Forme
{
    public partial class TelefonOperationForma : SBP_faza_2.Forme.AbstractOperationForm
    {
        public TelefonOperationForma()
        {
            InitializeComponent();
        }


        public override void KontroleUObjekat()
        {
            try
            {
                Validator.ValidirajTelefon(txtBrojTelefona.Text);
                Dto = new TelefonDTO(0, Int32.Parse(txtBrojTelefona.Text));
            }

            catch(ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }
        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto!= null)
            {
                TelefonDTO t = (TelefonDTO)dto;
                txtBrojTelefona.Text = t.Telefon.ToString();
            }
        }

    }
}
