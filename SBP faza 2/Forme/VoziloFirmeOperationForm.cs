﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ValidatorPodataka;
using SBP_faza_2.DTOs;

namespace SBP_faza_2.Forme
{
    public partial class VoziloFirmeOperationForm : AbstractOperationForm
    {
        public VoziloFirmeOperationForm()
        {
            InitializeComponent();
        }

        public override void KontroleUObjekat()
        {
            try
            {
                Validator.ValidirajGenericString(txtMarka.Text, "Marka");
                Validator.ValidirajGenericString(txtTip.Text, "Tip");
                Validator.ValidirajGodiste(txtGodiste.Text);
                Validator.ValidirajRegistraciju(txtRegistarskaOznaka.Text);
                base.Dto = new VoziloFirmeLongDTO()
                {
                    Marka = txtMarka.Text,
                    Tip = txtTip.Text,
                    Godiste = Int32.Parse(txtGodiste.Text),
                    RegistarskaOznaka = txtRegistarskaOznaka.Text,
                    IstekRegistracije = dtpIstekRegistracije.Value.Date
                };

            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }

        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto != null)
            {
                VoziloFirmeLongDTO v = (VoziloFirmeLongDTO)dto;
                txtMarka.Text = v.Marka;
                txtTip.Text = v.Tip;
                txtGodiste.Text = v.Godiste.ToString();
                txtRegistarskaOznaka.Text = v.RegistarskaOznaka;
                dtpIstekRegistracije.Value = v.IstekRegistracije;
                
            }
        }
    }
}
