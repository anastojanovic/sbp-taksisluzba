﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using SBP_faza_2.DTOManagers;


namespace SBP_faza_2.Forme
{
    public partial class VoziloListForm : SBP_faza_2.Forme.ListForm
    {
        VozacBasicDTO vozac;
        VoziloFirmeDTOManager dtoManager;
        UpravljaDTOManager upravljaDtoManager;
        VoziloFirmeOperationForm forma;
        VozacUpravljaForm dodavanjeVozila;
        public VoziloListForm(VozacBasicDTO dto)
        {
            InitializeComponent();
            this.vozac = dto;
            lblVozac.Text = "Vozac: " + vozac.ToString();
            dtoManager = new VoziloFirmeDTOManager();
            upravljaDtoManager = new UpravljaDTOManager();
            forma = new VoziloFirmeOperationForm();
            InitializeList();
        }

        protected override void InitializeList()
        {
            IEnumerable<AbstractDTO> lista = dtoManager.ReadAll((int)vozac.Id);
            getListBox().DataSource = lista;
            getListBox().Refresh();
        }
        protected override void Create()
        {

            VoziloFirmeLongDTO dto = null;
            forma.Dto = null;
            if (forma.ShowDialog() == DialogResult.OK)
            {
                dto = (VoziloFirmeLongDTO)forma.Dto;
                dto.Id = (int)vozac.Id;
                dtoManager.Create(dto);
                InitializeList();
            }

        }
        protected override void Read()
        {
            VoziloFirmeBasicDTO dto = (VoziloFirmeBasicDTO)getSelectedItem();
            VoziloFirmeLongDTO detailDto = (VoziloFirmeLongDTO)dtoManager.Read(dto.Id);
            setLblMoreInfo(detailDto.ToString());
        }

        protected override void Delete()
        {
            VoziloFirmeBasicDTO dto = (VoziloFirmeBasicDTO)getSelectedItem();

            if (MessageBox.Show("Da li ste sigurni da zelite da obrisete ovo vozilo iz vlasnistva vozaca?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                VoziloFirmeBasicDTO zaBrisanje = (VoziloFirmeBasicDTO)getSelectedItem();

                //dtoManager.ObrisiIzListeVozaca(vozac.Id, zaBrisanje.Id);
                upravljaDtoManager.Delete(vozac.Id, zaBrisanje.Id);
                InitializeList();
            }
        }

        protected override void Update()
        {
            VoziloFirmeBasicDTO dto = (VoziloFirmeBasicDTO)getSelectedItem();
            VoziloFirmeLongDTO detailDto = (VoziloFirmeLongDTO)dtoManager.Read(dto.Id);
            forma.ObjekatUKontrole(detailDto);

            VoziloFirmeLongDTO adto;

            if (forma.ShowDialog() == DialogResult.OK)
            {
                adto = (VoziloFirmeLongDTO)forma.Dto;
                adto.Id = dto.Id;
                dtoManager.Update(adto);
                InitializeList();

            }

            forma.Dto = null;
        }

        private void btnDodajVozila_Click(object sender, EventArgs e)
        {
            dodavanjeVozila = new VozacUpravljaForm((int)vozac.Id);
            if(dodavanjeVozila.ShowDialog()== DialogResult.OK)
            {
                InitializeList();
            }
        }
    }
}
