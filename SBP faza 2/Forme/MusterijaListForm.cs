﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;
namespace SBP_faza_2.Forme
{
    public partial class MusterijaListForm : SBP_faza_2.Forme.ListForm
    {
        public MusterijaListForm() : base(new MusterijaDTOManager(), new MusterijaOperationForm())
        {
            InitializeComponent();
            ListBox tmp = getListBox();
            if (tmp.Items.Count == 0)
                setLblMoreInfo("Ova musterija nema memorisane telefone");
        }

        private void btnTelefoni_Click(object sender, EventArgs e)
        {
            MusterijaBasicDTO dto = (MusterijaBasicDTO)getSelectedItem();
            TelefonMusterijeListForm forma = new TelefonMusterijeListForm(dto);
            forma.Show();
        }
    }
}
