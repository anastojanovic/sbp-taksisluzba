﻿using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SBP_faza_2.Forme
{
    public partial class LicnoVoziloListForm : SBP_faza_2.Forme.ListForm
    {
        VozacBasicDTO vozac;
        LicnoVoziloDTOManager dtoManager;
        LicnoVoziloOperationsForm forma;

        public LicnoVoziloListForm()
        {
            InitializeComponent();
        }

        public LicnoVoziloListForm(VozacBasicDTO dto)
        {
            InitializeComponent();
            this.vozac = dto;
            lblVozac.Text = "Vozac: " + vozac.ToString();
            dtoManager = new LicnoVoziloDTOManager();
            forma = new LicnoVoziloOperationsForm();
            InitializeList();
        }

        protected override void InitializeList()
        {
            IEnumerable<AbstractDTO> lista = dtoManager.ReadAll((int)vozac.Id);
            getListBox().DataSource = lista;
            getListBox().Refresh();
        }

        protected override void Create()
        {
            
            LicnoVoziloLongDTO dto = null;
            forma.Dto = null;
            if (forma.ShowDialog() == DialogResult.OK)
            {
                dto = (LicnoVoziloLongDTO)forma.Dto;
                dto.IdVozaca = (int)vozac.Id;
                dtoManager.Create(dto);
                InitializeList();
            }
            
        }

        protected override void Update()
        {
            LicnoVoziloShortDTO dto =  (LicnoVoziloShortDTO)getSelectedItem();
            LicnoVoziloLongDTO detailDto = dtoManager.Read(dto.IdVozila);
            forma.ObjekatUKontrole(detailDto);

            LicnoVoziloLongDTO adto;

            if (forma.ShowDialog() == DialogResult.OK)
            {
                adto = (LicnoVoziloLongDTO)forma.Dto;
                adto.IdVozila = dto.IdVozila;
                dtoManager.Update(adto);
                InitializeList();

            }

            forma.Dto = null;
        }

        protected override void Delete()
        {
            LicnoVoziloShortDTO dto = (LicnoVoziloShortDTO)getSelectedItem();

            if (MessageBox.Show("Da li ste sigurni da zelite da obrisete?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                dtoManager.Delete(dto.IdVozila);
                InitializeList();
            }
        }

        protected override void Read()
        {
            LicnoVoziloShortDTO dto = (LicnoVoziloShortDTO)getSelectedItem();
            LicnoVoziloLongDTO detailDto = dtoManager.Read(dto.IdVozila);
            setLblMoreInfo(detailDto.ToString());
        }

    }
}
