﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using ValidatorPodataka;

namespace SBP_faza_2.Forme
{
    public partial class AdminOperationForm : AbstractOperationForm
    {
        public AdminOperationForm()
        {
            InitializeComponent();         
        }

        public override void KontroleUObjekat()
        {
            try
            {
                Validator.ValidirajIme(txtIme.Text);
                Validator.ValidirajSlovo(txtSrednjeSlovo.Text);
                Validator.ValidirajIme(txtPrezime.Text);
                Validator.ValidirajAdresu(txtAdresa.Text);
                Validator.ValidirajTelefon(txtTelefon.Text);
                Validator.ValidirajGenericBroj(txtJMBG.Text, "Jmbg");
                Validator.ValidirajGenericString(txtStrucnaSprema.Text, "strucna sprema");
                base.Dto = new AdministratorLongDTO()
                {
                    LicnoIme = txtIme.Text,
                    SrednjeSlovo = txtSrednjeSlovo.Text,
                    Prezime = txtPrezime.Text,
                    JMBG = Int64.Parse(txtJMBG.Text),
                    Adresa = txtAdresa.Text,
                    Telefon = Int32.Parse(txtTelefon.Text),
                    StrucnaSprema = txtStrucnaSprema.Text
                };

            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }

        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto != null)
            {
                AdministratorLongDTO a = (AdministratorLongDTO)dto;
                txtIme.Text = a.LicnoIme;
                txtSrednjeSlovo.Text = a.SrednjeSlovo;
                txtPrezime.Text = a.Prezime;
                txtJMBG.Text = a.JMBG.ToString();
                txtAdresa.Text = a.Adresa;
                txtTelefon.Text = a.Telefon.ToString();
                txtStrucnaSprema.Text = a.StrucnaSprema;
            }
        }


        /*
        private void btnZapamti_Click(object sender, EventArgs e)
        {
            KontroleUObjekat();
            if (base.Dto!= null)
            {
                btnZapamti.DialogResult = DialogResult.OK;
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
           if( MessageBox.Show("Da li ste sigurni da zelite da zatvorite formu?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                btnOdustani.DialogResult = DialogResult.Cancel;
            }
        }
        */
    }
}
