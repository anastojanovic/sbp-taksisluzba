﻿namespace SBP_faza_2.Forme
{
    partial class MainForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdmin = new System.Windows.Forms.Button();
            this.btnVozac = new System.Windows.Forms.Button();
            this.btnVozilaFirme = new System.Windows.Forms.Button();
            this.btnPrikaziMusterije = new System.Windows.Forms.Button();
            this.btnPrikaziVoznje = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(70, 30);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(158, 33);
            this.btnAdmin.TabIndex = 10;
            this.btnAdmin.Text = "Prikaži administratore";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // btnVozac
            // 
            this.btnVozac.Location = new System.Drawing.Point(70, 80);
            this.btnVozac.Name = "btnVozac";
            this.btnVozac.Size = new System.Drawing.Size(158, 33);
            this.btnVozac.TabIndex = 11;
            this.btnVozac.Text = "Prikaži vozače";
            this.btnVozac.UseVisualStyleBackColor = true;
            this.btnVozac.Click += new System.EventHandler(this.btnVozac_Click);
            // 
            // btnVozilaFirme
            // 
            this.btnVozilaFirme.Location = new System.Drawing.Point(70, 132);
            this.btnVozilaFirme.Name = "btnVozilaFirme";
            this.btnVozilaFirme.Size = new System.Drawing.Size(158, 33);
            this.btnVozilaFirme.TabIndex = 12;
            this.btnVozilaFirme.Text = "Prikaži vozila firme";
            this.btnVozilaFirme.UseVisualStyleBackColor = true;
            this.btnVozilaFirme.Click += new System.EventHandler(this.btnVozilaFirme_Click);
            // 
            // btnPrikaziMusterije
            // 
            this.btnPrikaziMusterije.Location = new System.Drawing.Point(70, 183);
            this.btnPrikaziMusterije.Name = "btnPrikaziMusterije";
            this.btnPrikaziMusterije.Size = new System.Drawing.Size(158, 33);
            this.btnPrikaziMusterije.TabIndex = 13;
            this.btnPrikaziMusterije.Text = "Prikaži mušterije";
            this.btnPrikaziMusterije.UseVisualStyleBackColor = true;
            this.btnPrikaziMusterije.Click += new System.EventHandler(this.btnPrikaziMusterije_Click);
            // 
            // btnPrikaziVoznje
            // 
            this.btnPrikaziVoznje.Location = new System.Drawing.Point(70, 236);
            this.btnPrikaziVoznje.Name = "btnPrikaziVoznje";
            this.btnPrikaziVoznje.Size = new System.Drawing.Size(158, 33);
            this.btnPrikaziVoznje.TabIndex = 14;
            this.btnPrikaziVoznje.Text = "Prikaži vožnje";
            this.btnPrikaziVoznje.UseVisualStyleBackColor = true;
            this.btnPrikaziVoznje.Click += new System.EventHandler(this.btnPrikaziVoznje_Click);
            // 
            // MainForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 311);
            this.Controls.Add(this.btnPrikaziVoznje);
            this.Controls.Add(this.btnPrikaziMusterije);
            this.Controls.Add(this.btnVozilaFirme);
            this.Controls.Add(this.btnVozac);
            this.Controls.Add(this.btnAdmin);
            this.Name = "MainForma";
            this.Text = "Entiteti";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Button btnVozac;
        private System.Windows.Forms.Button btnVozilaFirme;
        private System.Windows.Forms.Button btnPrikaziMusterije;
        private System.Windows.Forms.Button btnPrikaziVoznje;
    }
}

