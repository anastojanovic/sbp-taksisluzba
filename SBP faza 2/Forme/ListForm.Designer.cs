﻿namespace SBP_faza_2.Forme
{
    partial class ListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbxEntiteti = new System.Windows.Forms.ListBox();
            this.lblListaEntiteta = new System.Windows.Forms.Label();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnAzuriraj = new System.Windows.Forms.Button();
            this.lblMoreInfo = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // lbxEntiteti
            // 
            this.lbxEntiteti.FormattingEnabled = true;
            this.lbxEntiteti.HorizontalScrollbar = true;
            this.lbxEntiteti.ItemHeight = 16;
            this.lbxEntiteti.Location = new System.Drawing.Point(20, 56);
            this.lbxEntiteti.Name = "lbxEntiteti";
            this.lbxEntiteti.Size = new System.Drawing.Size(393, 196);
            this.lbxEntiteti.TabIndex = 2;
            this.lbxEntiteti.SelectedIndexChanged += new System.EventHandler(this.lbxEntiteti_SelectedIndexChanged);
            // 
            // lblListaEntiteta
            // 
            this.lblListaEntiteta.AutoSize = true;
            this.lblListaEntiteta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListaEntiteta.Location = new System.Drawing.Point(23, 20);
            this.lblListaEntiteta.Name = "lblListaEntiteta";
            this.lblListaEntiteta.Size = new System.Drawing.Size(120, 20);
            this.lblListaEntiteta.TabIndex = 3;
            this.lblListaEntiteta.Text = "Lista entiteta";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(20, 280);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(113, 23);
            this.btnDodaj.TabIndex = 4;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(300, 280);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(113, 23);
            this.btnObrisi.TabIndex = 6;
            this.btnObrisi.Text = "Obriši";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnAzuriraj
            // 
            this.btnAzuriraj.Location = new System.Drawing.Point(160, 280);
            this.btnAzuriraj.Name = "btnAzuriraj";
            this.btnAzuriraj.Size = new System.Drawing.Size(113, 23);
            this.btnAzuriraj.TabIndex = 7;
            this.btnAzuriraj.Text = "Ažuriraj";
            this.btnAzuriraj.UseVisualStyleBackColor = true;
            this.btnAzuriraj.Click += new System.EventHandler(this.btnAzuriraj_Click);
            // 
            // lblMoreInfo
            // 
            this.lblMoreInfo.AutoSize = true;
            this.lblMoreInfo.Location = new System.Drawing.Point(434, 56);
            this.lblMoreInfo.Name = "lblMoreInfo";
            this.lblMoreInfo.Size = new System.Drawing.Size(67, 17);
            this.lblMoreInfo.TabIndex = 8;
            this.lblMoreInfo.Text = "More info";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // ListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 333);
            this.Controls.Add(this.lblMoreInfo);
            this.Controls.Add(this.btnAzuriraj);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.lblListaEntiteta);
            this.Controls.Add(this.lbxEntiteti);
            this.Name = "ListForm";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.Text = "Lista Entiteta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lbxEntiteti;
        private System.Windows.Forms.Label lblListaEntiteta;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnAzuriraj;
        private System.Windows.Forms.Label lblMoreInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}