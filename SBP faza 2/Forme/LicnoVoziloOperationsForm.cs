﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using ValidatorPodataka;

namespace SBP_faza_2.Forme
{
    public partial class LicnoVoziloOperationsForm : SBP_faza_2.Forme.AbstractOperationForm
    {
        public LicnoVoziloOperationsForm()
        {
            InitializeComponent();
        }

        public override void KontroleUObjekat()
        {
            try
            {
                Validator.ValidirajGenericString(txtMarka.Text, "Marka");
                Validator.ValidirajGenericString(txtTip.Text, "Tip vozila");
                Validator.ValidirajGenericString(txtBoja.Text, "Boja");
                Validator.ValidirajDatum(dtpDatumOd.Value, dtpDatumDo.Value);
                Dto = new LicnoVoziloLongDTO(0, 0, txtMarka.Text, txtTip.Text, txtBoja.Text, dtpDatumOd.Value, dtpDatumDo.Value);

            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
                base.Dto = null;
            }

        }

        public override void ObjekatUKontrole(AbstractDTO dto)
        {
            if (dto != null)
            {
                LicnoVoziloLongDTO a = (LicnoVoziloLongDTO)dto;
                txtBoja.Text = a.Boja;
                txtMarka.Text = a.Marka;
                txtTip.Text = a.Tip;
                dtpDatumOd.Value = a.DatumOd;
                dtpDatumDo.Value = a.DatumDo;
            }
        }
    }
}
