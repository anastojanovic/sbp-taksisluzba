﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;
namespace SBP_faza_2.Forme
{
    public partial class ListForm : Form
    {
        IAbstractDTOManager dtoManager;
        AbstractOperationForm forma;

        public ListForm()
        {
            InitializeComponent();
        }

        public ListForm(IAbstractDTOManager dtoManager, AbstractOperationForm forma)
        {
            InitializeComponent();
            this.dtoManager = dtoManager;
            this.forma = forma; 
            InitializeList();
        }

        #region Template Methods

        protected virtual void InitializeList()
        {           
            IEnumerable<AbstractDTO> lista= dtoManager.ReadAll();
            lbxEntiteti.DataSource = lista;
            lbxEntiteti.Refresh();
        }

        protected virtual void Create()
        {
            AbstractDTO dto = null;
            forma.Dto = null;
            if (forma.ShowDialog() == DialogResult.OK)
            {
                dto = forma.Dto;
                dtoManager.Create(dto);
                InitializeList();
            }
        }

        protected new virtual void Update()
        {
            AbstractDTO dto = (AbstractDTO)lbxEntiteti.SelectedItem;
            AbstractDTO detailDto = dtoManager.Read(dto.Id);
            forma.ObjekatUKontrole(detailDto);

            AbstractDTO adto;

            if (forma.ShowDialog() == DialogResult.OK)
            {
                adto = forma.Dto;
                adto.Id = dto.Id;
                dtoManager.Update(adto);
                InitializeList();

            }

            forma.Dto = null;
        }

        protected virtual void Delete()
        {
            AbstractDTO dto = (AbstractDTO)lbxEntiteti.SelectedItem;

            if (MessageBox.Show("Da li ste sigurni da zelite da obrisete?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                dtoManager.Delete(dto.Id);
                InitializeList();
            }
        }

        protected virtual void Read()
        {
            AbstractDTO dto = (AbstractDTO)lbxEntiteti.SelectedItem;
            AbstractDTO detailDto = dtoManager.Read(dto.Id);
            lblMoreInfo.Text = detailDto.ToString();
        }

        protected AbstractDTO getSelectedItem()
        {
            if(lbxEntiteti.SelectedItem != null)
                return (AbstractDTO)lbxEntiteti.SelectedItem;
            return null;
        }

        protected ListBox getListBox()
        {
            return lbxEntiteti;
        }

        protected void setLblMoreInfo(String text)
        {
            lblMoreInfo.Text = text;
        }

        #endregion


        #region On Click Listeners

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            Create();           
        }
        public void SakrijAzurirajDugme()
        {
            btnAzuriraj.Enabled = false;
            btnAzuriraj.Visible = false;
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            Update();                   
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void lbxEntiteti_SelectedIndexChanged(object sender, EventArgs e)
        {
            Read();
        }

        #endregion
    }
}
