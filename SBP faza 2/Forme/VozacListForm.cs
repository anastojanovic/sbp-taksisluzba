﻿using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SBP_faza_2.Forme
{
    public partial class VozacListForm : SBP_faza_2.Forme.ListForm
    {

        public VozacListForm()
            :base (new VozacDTOManager(), new VozacOperationForm())
        {
            InitializeComponent();
        }

        private void btnLicnaVozila_Click(object sender, EventArgs e)
        {
            VozacBasicDTO vozac = (VozacBasicDTO)getSelectedItem();
            LicnoVoziloListForm forma = new LicnoVoziloListForm(vozac);
            forma.Show();
        }

        private void btnVozilaFirme_Click(object sender, EventArgs e)
        {
            VozacBasicDTO vozac = (VozacBasicDTO)getSelectedItem();
            VoziloListForm forma = new VoziloListForm(vozac);
            forma.Show();
        }
    }
}
