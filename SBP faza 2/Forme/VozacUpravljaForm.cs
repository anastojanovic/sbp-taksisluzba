﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SBP_faza_2.DTOs;
using SBP_faza_2.DTOManagers;
namespace SBP_faza_2.Forme
{
    public partial class VozacUpravljaForm : Form
    {
        VoziloFirmeDTOManager dtoM;
        UpravljaDTOManager upravljaDTOM;
        AbstractDTOEqualityComparer comparer;
        int id;
   
        public VozacUpravljaForm(int idVozaca)
        {
            
            InitializeComponent();
            upravljaDTOM = new UpravljaDTOManager();
            dtoM = new VoziloFirmeDTOManager();
            comparer = new AbstractDTOEqualityComparer();
            id = idVozaca;
            InicijalizujListu();
        }

        public void InicijalizujListu()
        {
            IEnumerable<AbstractDTO> listaSvih = dtoM.ReadAll();
            IEnumerable<AbstractDTO> listaPostojecih = dtoM.ReadAll(id);
            IEnumerable<AbstractDTO> listaZaPrikaz = listaSvih.Except(listaPostojecih, comparer);
            

            lbxListaVozila.DataSource = listaZaPrikaz.ToList();
        }

        UpravljaDTO KontroleUObjekat()
        {
            try
            {
                VoziloFirmeBasicDTO v = (VoziloFirmeBasicDTO)lbxListaVozila.SelectedItem;
                if (v == null) throw new ArgumentException("Doslo je do greske pri odabiru vozila");
                int idV = (int)v.Id;
                ValidatorPodataka.Validator.ValidirajDatum(dtpDatumPocetka.Value, dtpDatumKraja.Value);
                UpravljaDTO u = new UpravljaDTO
                {
                    DatumDo = dtpDatumKraja.Value,
                    DatumOd = dtpDatumPocetka.Value,
                    IdVozaca = id,
                    IdVozilaFirme = idV

                };
                return u;
            }
            catch(ArgumentException e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }

        private void btnZapamti_Click(object sender, EventArgs e)
        {
            UpravljaDTO u = KontroleUObjekat();
            if(u != null)
            {
                upravljaDTOM.Create(u);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.None;
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da zelite da zatvorite formu?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this.DialogResult = DialogResult.Cancel;

            }
            else
            {
                this.DialogResult = DialogResult.None;
            }
        }

        private void VozacUpravljaForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
                e.Cancel = true;
        }
    }
}
