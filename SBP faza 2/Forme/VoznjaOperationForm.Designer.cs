﻿namespace SBP_faza_2.Forme
{
    partial class VoznjaOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPocetnaStanica = new System.Windows.Forms.Label();
            this.lblKrajnjaStanica = new System.Windows.Forms.Label();
            this.lblVremePocetka = new System.Windows.Forms.Label();
            this.lblVremeKraja = new System.Windows.Forms.Label();
            this.lblVremePrimanjaPoziva = new System.Windows.Forms.Label();
            this.lblPozivniTelefon = new System.Windows.Forms.Label();
            this.txtPocetnaStanica = new System.Windows.Forms.TextBox();
            this.txtKrajnjaStanica = new System.Windows.Forms.TextBox();
            this.dtpVremePrimanjaPoziva = new System.Windows.Forms.DateTimePicker();
            this.dtpVremeKraja = new System.Windows.Forms.DateTimePicker();
            this.dtpVremePocetka = new System.Windows.Forms.DateTimePicker();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.cbxVozaci = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.lblMusterija = new System.Windows.Forms.Label();
            this.cbxAdmini = new System.Windows.Forms.ComboBox();
            this.cbxMusterije = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPocetnaStanica
            // 
            this.lblPocetnaStanica.AutoSize = true;
            this.lblPocetnaStanica.Location = new System.Drawing.Point(30, 34);
            this.lblPocetnaStanica.Name = "lblPocetnaStanica";
            this.lblPocetnaStanica.Size = new System.Drawing.Size(113, 17);
            this.lblPocetnaStanica.TabIndex = 34;
            this.lblPocetnaStanica.Text = "Pocetna stanica:";
            // 
            // lblKrajnjaStanica
            // 
            this.lblKrajnjaStanica.AutoSize = true;
            this.lblKrajnjaStanica.Location = new System.Drawing.Point(30, 75);
            this.lblKrajnjaStanica.Name = "lblKrajnjaStanica";
            this.lblKrajnjaStanica.Size = new System.Drawing.Size(109, 17);
            this.lblKrajnjaStanica.TabIndex = 35;
            this.lblKrajnjaStanica.Text = "Krajnja stanica: ";
            // 
            // lblVremePocetka
            // 
            this.lblVremePocetka.AutoSize = true;
            this.lblVremePocetka.Location = new System.Drawing.Point(30, 120);
            this.lblVremePocetka.Name = "lblVremePocetka";
            this.lblVremePocetka.Size = new System.Drawing.Size(107, 17);
            this.lblVremePocetka.TabIndex = 36;
            this.lblVremePocetka.Text = "Vreme pocetka:";
            // 
            // lblVremeKraja
            // 
            this.lblVremeKraja.AutoSize = true;
            this.lblVremeKraja.Location = new System.Drawing.Point(30, 169);
            this.lblVremeKraja.Name = "lblVremeKraja";
            this.lblVremeKraja.Size = new System.Drawing.Size(92, 17);
            this.lblVremeKraja.TabIndex = 37;
            this.lblVremeKraja.Text = "Vreme kraja: ";
            // 
            // lblVremePrimanjaPoziva
            // 
            this.lblVremePrimanjaPoziva.AutoSize = true;
            this.lblVremePrimanjaPoziva.Location = new System.Drawing.Point(30, 211);
            this.lblVremePrimanjaPoziva.Name = "lblVremePrimanjaPoziva";
            this.lblVremePrimanjaPoziva.Size = new System.Drawing.Size(160, 17);
            this.lblVremePrimanjaPoziva.TabIndex = 38;
            this.lblVremePrimanjaPoziva.Text = "Vreme primanja poziva: ";
            // 
            // lblPozivniTelefon
            // 
            this.lblPozivniTelefon.AutoSize = true;
            this.lblPozivniTelefon.Location = new System.Drawing.Point(30, 257);
            this.lblPozivniTelefon.Name = "lblPozivniTelefon";
            this.lblPozivniTelefon.Size = new System.Drawing.Size(104, 17);
            this.lblPozivniTelefon.TabIndex = 39;
            this.lblPozivniTelefon.Text = "Pozivni telefon:";
            // 
            // txtPocetnaStanica
            // 
            this.txtPocetnaStanica.Location = new System.Drawing.Point(217, 37);
            this.txtPocetnaStanica.Name = "txtPocetnaStanica";
            this.txtPocetnaStanica.Size = new System.Drawing.Size(185, 22);
            this.txtPocetnaStanica.TabIndex = 40;
            // 
            // txtKrajnjaStanica
            // 
            this.txtKrajnjaStanica.Location = new System.Drawing.Point(217, 75);
            this.txtKrajnjaStanica.Name = "txtKrajnjaStanica";
            this.txtKrajnjaStanica.Size = new System.Drawing.Size(185, 22);
            this.txtKrajnjaStanica.TabIndex = 41;
            // 
            // dtpVremePrimanjaPoziva
            // 
            this.dtpVremePrimanjaPoziva.CustomFormat = "dd.MM.yyyy hh:ss";
            this.dtpVremePrimanjaPoziva.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVremePrimanjaPoziva.Location = new System.Drawing.Point(217, 211);
            this.dtpVremePrimanjaPoziva.Name = "dtpVremePrimanjaPoziva";
            this.dtpVremePrimanjaPoziva.Size = new System.Drawing.Size(185, 22);
            this.dtpVremePrimanjaPoziva.TabIndex = 45;
            // 
            // dtpVremeKraja
            // 
            this.dtpVremeKraja.CustomFormat = "dd.MM.yyyy hh:ss";
            this.dtpVremeKraja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVremeKraja.Location = new System.Drawing.Point(217, 164);
            this.dtpVremeKraja.Name = "dtpVremeKraja";
            this.dtpVremeKraja.Size = new System.Drawing.Size(185, 22);
            this.dtpVremeKraja.TabIndex = 46;
            // 
            // dtpVremePocetka
            // 
            this.dtpVremePocetka.CustomFormat = "dd.MM.yyyy hh:ss";
            this.dtpVremePocetka.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVremePocetka.Location = new System.Drawing.Point(217, 115);
            this.dtpVremePocetka.Name = "dtpVremePocetka";
            this.dtpVremePocetka.Size = new System.Drawing.Size(185, 22);
            this.dtpVremePocetka.TabIndex = 47;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(217, 257);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(185, 22);
            this.txtTelefon.TabIndex = 48;
            // 
            // cbxVozaci
            // 
            this.cbxVozaci.FormattingEnabled = true;
            this.cbxVozaci.Location = new System.Drawing.Point(217, 329);
            this.cbxVozaci.Name = "cbxVozaci";
            this.cbxVozaci.Size = new System.Drawing.Size(185, 24);
            this.cbxVozaci.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 50;
            this.label1.Text = "Vozač:";
            // 
            // lblAdmin
            // 
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Location = new System.Drawing.Point(30, 372);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(95, 17);
            this.lblAdmin.TabIndex = 51;
            this.lblAdmin.Text = "Administrator:";
            // 
            // lblMusterija
            // 
            this.lblMusterija.AutoSize = true;
            this.lblMusterija.Location = new System.Drawing.Point(30, 406);
            this.lblMusterija.Name = "lblMusterija";
            this.lblMusterija.Size = new System.Drawing.Size(65, 17);
            this.lblMusterija.TabIndex = 52;
            this.lblMusterija.Text = "Mušterija";
            // 
            // cbxAdmini
            // 
            this.cbxAdmini.FormattingEnabled = true;
            this.cbxAdmini.Location = new System.Drawing.Point(217, 365);
            this.cbxAdmini.Name = "cbxAdmini";
            this.cbxAdmini.Size = new System.Drawing.Size(185, 24);
            this.cbxAdmini.TabIndex = 53;
            // 
            // cbxMusterije
            // 
            this.cbxMusterije.FormattingEnabled = true;
            this.cbxMusterije.Location = new System.Drawing.Point(217, 406);
            this.cbxMusterije.Name = "cbxMusterije";
            this.cbxMusterije.Size = new System.Drawing.Size(185, 24);
            this.cbxMusterije.TabIndex = 54;
            // 
            // VoznjaOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(439, 503);
            this.Controls.Add(this.cbxMusterije);
            this.Controls.Add(this.cbxAdmini);
            this.Controls.Add(this.lblMusterija);
            this.Controls.Add(this.lblAdmin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxVozaci);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.dtpVremePocetka);
            this.Controls.Add(this.dtpVremeKraja);
            this.Controls.Add(this.dtpVremePrimanjaPoziva);
            this.Controls.Add(this.txtKrajnjaStanica);
            this.Controls.Add(this.txtPocetnaStanica);
            this.Controls.Add(this.lblPozivniTelefon);
            this.Controls.Add(this.lblVremePrimanjaPoziva);
            this.Controls.Add(this.lblVremeKraja);
            this.Controls.Add(this.lblVremePocetka);
            this.Controls.Add(this.lblKrajnjaStanica);
            this.Controls.Add(this.lblPocetnaStanica);
            this.Name = "VoznjaOperationForm";
            this.Controls.SetChildIndex(this.lblPocetnaStanica, 0);
            this.Controls.SetChildIndex(this.lblKrajnjaStanica, 0);
            this.Controls.SetChildIndex(this.lblVremePocetka, 0);
            this.Controls.SetChildIndex(this.lblVremeKraja, 0);
            this.Controls.SetChildIndex(this.lblVremePrimanjaPoziva, 0);
            this.Controls.SetChildIndex(this.lblPozivniTelefon, 0);
            this.Controls.SetChildIndex(this.txtPocetnaStanica, 0);
            this.Controls.SetChildIndex(this.txtKrajnjaStanica, 0);
            this.Controls.SetChildIndex(this.dtpVremePrimanjaPoziva, 0);
            this.Controls.SetChildIndex(this.dtpVremeKraja, 0);
            this.Controls.SetChildIndex(this.dtpVremePocetka, 0);
            this.Controls.SetChildIndex(this.txtTelefon, 0);
            this.Controls.SetChildIndex(this.cbxVozaci, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblAdmin, 0);
            this.Controls.SetChildIndex(this.lblMusterija, 0);
            this.Controls.SetChildIndex(this.cbxAdmini, 0);
            this.Controls.SetChildIndex(this.cbxMusterije, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPocetnaStanica;
        private System.Windows.Forms.Label lblKrajnjaStanica;
        private System.Windows.Forms.Label lblVremePocetka;
        private System.Windows.Forms.Label lblVremeKraja;
        private System.Windows.Forms.Label lblVremePrimanjaPoziva;
        private System.Windows.Forms.Label lblPozivniTelefon;
        private System.Windows.Forms.TextBox txtPocetnaStanica;
        private System.Windows.Forms.TextBox txtKrajnjaStanica;
        private System.Windows.Forms.DateTimePicker dtpVremePrimanjaPoziva;
        private System.Windows.Forms.DateTimePicker dtpVremeKraja;
        private System.Windows.Forms.DateTimePicker dtpVremePocetka;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.ComboBox cbxVozaci;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAdmin;
        private System.Windows.Forms.Label lblMusterija;
        private System.Windows.Forms.ComboBox cbxAdmini;
        private System.Windows.Forms.ComboBox cbxMusterije;
    }
}
