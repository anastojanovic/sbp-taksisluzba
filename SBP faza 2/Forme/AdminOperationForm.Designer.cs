﻿namespace SBP_faza_2.Forme
{
    partial class AdminOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSrednjeSlovo = new System.Windows.Forms.TextBox();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtJMBG = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtStrucnaSprema = new System.Windows.Forms.TextBox();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.lblJMBG = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblStrucnaSprema = new System.Windows.Forms.Label();
            this.btnStrucnaSprema = new System.Windows.Forms.Label();
            this.lblSrednjeSlovo = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtSrednjeSlovo
            // 
            this.txtSrednjeSlovo.Location = new System.Drawing.Point(186, 101);
            this.txtSrednjeSlovo.Name = "txtSrednjeSlovo";
            this.txtSrednjeSlovo.Size = new System.Drawing.Size(156, 22);
            this.txtSrednjeSlovo.TabIndex = 29;
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(186, 137);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(156, 22);
            this.txtPrezime.TabIndex = 28;
            // 
            // txtJMBG
            // 
            this.txtJMBG.Location = new System.Drawing.Point(186, 174);
            this.txtJMBG.Name = "txtJMBG";
            this.txtJMBG.Size = new System.Drawing.Size(156, 22);
            this.txtJMBG.TabIndex = 27;
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(186, 212);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(156, 22);
            this.txtAdresa.TabIndex = 26;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(186, 252);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(156, 22);
            this.txtTelefon.TabIndex = 25;
            // 
            // txtStrucnaSprema
            // 
            this.txtStrucnaSprema.Location = new System.Drawing.Point(186, 290);
            this.txtStrucnaSprema.Name = "txtStrucnaSprema";
            this.txtStrucnaSprema.Size = new System.Drawing.Size(156, 22);
            this.txtStrucnaSprema.TabIndex = 24;
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(186, 62);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(156, 22);
            this.txtIme.TabIndex = 23;
            // 
            // lblJMBG
            // 
            this.lblJMBG.AutoSize = true;
            this.lblJMBG.Location = new System.Drawing.Point(29, 174);
            this.lblJMBG.Name = "lblJMBG";
            this.lblJMBG.Size = new System.Drawing.Size(50, 17);
            this.lblJMBG.TabIndex = 22;
            this.lblJMBG.Text = "JMBG:";
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Location = new System.Drawing.Point(29, 252);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(60, 17);
            this.lblTelefon.TabIndex = 21;
            this.lblTelefon.Text = "Telefon:";
            // 
            // lblStrucnaSprema
            // 
            this.lblStrucnaSprema.AutoSize = true;
            this.lblStrucnaSprema.Location = new System.Drawing.Point(29, 290);
            this.lblStrucnaSprema.Name = "lblStrucnaSprema";
            this.lblStrucnaSprema.Size = new System.Drawing.Size(112, 17);
            this.lblStrucnaSprema.TabIndex = 20;
            this.lblStrucnaSprema.Text = "Strucna sprema:";
            // 
            // btnStrucnaSprema
            // 
            this.btnStrucnaSprema.AutoSize = true;
            this.btnStrucnaSprema.Location = new System.Drawing.Point(29, 212);
            this.btnStrucnaSprema.Name = "btnStrucnaSprema";
            this.btnStrucnaSprema.Size = new System.Drawing.Size(57, 17);
            this.btnStrucnaSprema.TabIndex = 19;
            this.btnStrucnaSprema.Text = "Adresa:";
            // 
            // lblSrednjeSlovo
            // 
            this.lblSrednjeSlovo.AutoSize = true;
            this.lblSrednjeSlovo.Location = new System.Drawing.Point(29, 101);
            this.lblSrednjeSlovo.Name = "lblSrednjeSlovo";
            this.lblSrednjeSlovo.Size = new System.Drawing.Size(102, 17);
            this.lblSrednjeSlovo.TabIndex = 18;
            this.lblSrednjeSlovo.Text = "Srednje slovo: ";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(29, 137);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(63, 17);
            this.lblPrezime.TabIndex = 17;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(29, 62);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(38, 17);
            this.lblIme.TabIndex = 16;
            this.lblIme.Text = "Ime: ";
            // 
            // AdminOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(370, 528);
            this.Controls.Add(this.txtSrednjeSlovo);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtJMBG);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.txtStrucnaSprema);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.lblJMBG);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.lblStrucnaSprema);
            this.Controls.Add(this.btnStrucnaSprema);
            this.Controls.Add(this.lblSrednjeSlovo);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.Name = "AdminOperationForm";
            this.Controls.SetChildIndex(this.lblIme, 0);
            this.Controls.SetChildIndex(this.lblPrezime, 0);
            this.Controls.SetChildIndex(this.lblSrednjeSlovo, 0);
            this.Controls.SetChildIndex(this.btnStrucnaSprema, 0);
            this.Controls.SetChildIndex(this.lblStrucnaSprema, 0);
            this.Controls.SetChildIndex(this.lblTelefon, 0);
            this.Controls.SetChildIndex(this.lblJMBG, 0);
            this.Controls.SetChildIndex(this.txtIme, 0);
            this.Controls.SetChildIndex(this.txtStrucnaSprema, 0);
            this.Controls.SetChildIndex(this.txtTelefon, 0);
            this.Controls.SetChildIndex(this.txtAdresa, 0);
            this.Controls.SetChildIndex(this.txtJMBG, 0);
            this.Controls.SetChildIndex(this.txtPrezime, 0);
            this.Controls.SetChildIndex(this.txtSrednjeSlovo, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtSrednjeSlovo;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtJMBG;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.TextBox txtStrucnaSprema;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label lblJMBG;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblStrucnaSprema;
        private System.Windows.Forms.Label btnStrucnaSprema;
        private System.Windows.Forms.Label lblSrednjeSlovo;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblIme;
    }
}
