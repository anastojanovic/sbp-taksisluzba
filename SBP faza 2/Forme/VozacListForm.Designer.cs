﻿namespace SBP_faza_2.Forme
{
    partial class VozacListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLicnaVozila = new System.Windows.Forms.Button();
            this.btnVozilaFirme = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLicnaVozila
            // 
            this.btnLicnaVozila.Location = new System.Drawing.Point(20, 328);
            this.btnLicnaVozila.Name = "btnLicnaVozila";
            this.btnLicnaVozila.Size = new System.Drawing.Size(113, 23);
            this.btnLicnaVozila.TabIndex = 9;
            this.btnLicnaVozila.Text = "Licna Vozila";
            this.btnLicnaVozila.UseVisualStyleBackColor = true;
            this.btnLicnaVozila.Click += new System.EventHandler(this.btnLicnaVozila_Click);
            // 
            // btnVozilaFirme
            // 
            this.btnVozilaFirme.Location = new System.Drawing.Point(160, 328);
            this.btnVozilaFirme.Name = "btnVozilaFirme";
            this.btnVozilaFirme.Size = new System.Drawing.Size(113, 23);
            this.btnVozilaFirme.TabIndex = 10;
            this.btnVozilaFirme.Text = "Vozila Firme";
            this.btnVozilaFirme.UseVisualStyleBackColor = true;
            this.btnVozilaFirme.Click += new System.EventHandler(this.btnVozilaFirme_Click);
            // 
            // VozacListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(702, 374);
            this.Controls.Add(this.btnVozilaFirme);
            this.Controls.Add(this.btnLicnaVozila);
            this.Name = "VozacListForm";
            this.Controls.SetChildIndex(this.btnLicnaVozila, 0);
            this.Controls.SetChildIndex(this.btnVozilaFirme, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLicnaVozila;
        private System.Windows.Forms.Button btnVozilaFirme;
    }
}
