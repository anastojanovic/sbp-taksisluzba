﻿namespace SBP_faza_2.Forme
{
    partial class VoziloFirmeOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMarka = new System.Windows.Forms.TextBox();
            this.txtTip = new System.Windows.Forms.TextBox();
            this.txtGodiste = new System.Windows.Forms.TextBox();
            this.txtRegistarskaOznaka = new System.Windows.Forms.TextBox();
            this.lblMarka = new System.Windows.Forms.Label();
            this.lblTip = new System.Windows.Forms.Label();
            this.lblGodiste = new System.Windows.Forms.Label();
            this.lblRegistarskaOznaka = new System.Windows.Forms.Label();
            this.lblIstekRegistracije = new System.Windows.Forms.Label();
            this.dtpIstekRegistracije = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // txtMarka
            // 
            this.txtMarka.Location = new System.Drawing.Point(228, 147);
            this.txtMarka.Name = "txtMarka";
            this.txtMarka.Size = new System.Drawing.Size(100, 22);
            this.txtMarka.TabIndex = 34;
            // 
            // txtTip
            // 
            this.txtTip.Location = new System.Drawing.Point(228, 184);
            this.txtTip.Name = "txtTip";
            this.txtTip.Size = new System.Drawing.Size(100, 22);
            this.txtTip.TabIndex = 35;
            // 
            // txtGodiste
            // 
            this.txtGodiste.Location = new System.Drawing.Point(228, 222);
            this.txtGodiste.Name = "txtGodiste";
            this.txtGodiste.Size = new System.Drawing.Size(100, 22);
            this.txtGodiste.TabIndex = 36;
            // 
            // txtRegistarskaOznaka
            // 
            this.txtRegistarskaOznaka.Location = new System.Drawing.Point(228, 260);
            this.txtRegistarskaOznaka.Name = "txtRegistarskaOznaka";
            this.txtRegistarskaOznaka.Size = new System.Drawing.Size(100, 22);
            this.txtRegistarskaOznaka.TabIndex = 37;
            // 
            // lblMarka
            // 
            this.lblMarka.AutoSize = true;
            this.lblMarka.Location = new System.Drawing.Point(35, 152);
            this.lblMarka.Name = "lblMarka";
            this.lblMarka.Size = new System.Drawing.Size(51, 17);
            this.lblMarka.TabIndex = 39;
            this.lblMarka.Text = "Marka:";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(35, 189);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(32, 17);
            this.lblTip.TabIndex = 40;
            this.lblTip.Text = "Tip:";
            // 
            // lblGodiste
            // 
            this.lblGodiste.AutoSize = true;
            this.lblGodiste.Location = new System.Drawing.Point(35, 222);
            this.lblGodiste.Name = "lblGodiste";
            this.lblGodiste.Size = new System.Drawing.Size(61, 17);
            this.lblGodiste.TabIndex = 41;
            this.lblGodiste.Text = "Godište:";
            // 
            // lblRegistarskaOznaka
            // 
            this.lblRegistarskaOznaka.AutoSize = true;
            this.lblRegistarskaOznaka.Location = new System.Drawing.Point(35, 260);
            this.lblRegistarskaOznaka.Name = "lblRegistarskaOznaka";
            this.lblRegistarskaOznaka.Size = new System.Drawing.Size(137, 17);
            this.lblRegistarskaOznaka.TabIndex = 42;
            this.lblRegistarskaOznaka.Text = "Registarska oznaka:";
            // 
            // lblIstekRegistracije
            // 
            this.lblIstekRegistracije.AutoSize = true;
            this.lblIstekRegistracije.Location = new System.Drawing.Point(35, 298);
            this.lblIstekRegistracije.Name = "lblIstekRegistracije";
            this.lblIstekRegistracije.Size = new System.Drawing.Size(171, 17);
            this.lblIstekRegistracije.TabIndex = 43;
            this.lblIstekRegistracije.Text = "Datum isteka registracije: ";
            // 
            // dtpIstekRegistracije
            // 
            this.dtpIstekRegistracije.CustomFormat = "dd/MM/yyyy";
            this.dtpIstekRegistracije.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIstekRegistracije.Location = new System.Drawing.Point(228, 298);
            this.dtpIstekRegistracije.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dtpIstekRegistracije.MinDate = new System.DateTime(1900, 7, 1, 0, 0, 0, 0);
            this.dtpIstekRegistracije.Name = "dtpIstekRegistracije";
            this.dtpIstekRegistracije.Size = new System.Drawing.Size(100, 22);
            this.dtpIstekRegistracije.TabIndex = 44;
            this.dtpIstekRegistracije.Value = new System.DateTime(2018, 7, 1, 0, 0, 0, 0);
            // 
            // VoziloFirmeOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(375, 516);
            this.Controls.Add(this.dtpIstekRegistracije);
            this.Controls.Add(this.lblIstekRegistracije);
            this.Controls.Add(this.lblRegistarskaOznaka);
            this.Controls.Add(this.lblGodiste);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.lblMarka);
            this.Controls.Add(this.txtRegistarskaOznaka);
            this.Controls.Add(this.txtGodiste);
            this.Controls.Add(this.txtTip);
            this.Controls.Add(this.txtMarka);
            this.Name = "VoziloFirmeOperationForm";
            this.Controls.SetChildIndex(this.txtMarka, 0);
            this.Controls.SetChildIndex(this.txtTip, 0);
            this.Controls.SetChildIndex(this.txtGodiste, 0);
            this.Controls.SetChildIndex(this.txtRegistarskaOznaka, 0);
            this.Controls.SetChildIndex(this.lblMarka, 0);
            this.Controls.SetChildIndex(this.lblTip, 0);
            this.Controls.SetChildIndex(this.lblGodiste, 0);
            this.Controls.SetChildIndex(this.lblRegistarskaOznaka, 0);
            this.Controls.SetChildIndex(this.lblIstekRegistracije, 0);
            this.Controls.SetChildIndex(this.dtpIstekRegistracije, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMarka;
        private System.Windows.Forms.TextBox txtTip;
        private System.Windows.Forms.TextBox txtGodiste;
        private System.Windows.Forms.TextBox txtRegistarskaOznaka;
        private System.Windows.Forms.Label lblMarka;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.Label lblGodiste;
        private System.Windows.Forms.Label lblRegistarskaOznaka;
        private System.Windows.Forms.Label lblIstekRegistracije;
        private System.Windows.Forms.DateTimePicker dtpIstekRegistracije;
    }
}
