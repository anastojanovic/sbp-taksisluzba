﻿namespace SBP_faza_2.Forme
{
    partial class VozacUpravljaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxListaVozila = new System.Windows.Forms.ListBox();
            this.dtpDatumPocetka = new System.Windows.Forms.DateTimePicker();
            this.dtpDatumKraja = new System.Windows.Forms.DateTimePicker();
            this.lblDatumPocetka = new System.Windows.Forms.Label();
            this.lblDatumKraja = new System.Windows.Forms.Label();
            this.btnOdustani = new System.Windows.Forms.Button();
            this.btnZapamti = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbxListaVozila
            // 
            this.lbxListaVozila.FormattingEnabled = true;
            this.lbxListaVozila.ItemHeight = 16;
            this.lbxListaVozila.Location = new System.Drawing.Point(45, 35);
            this.lbxListaVozila.Name = "lbxListaVozila";
            this.lbxListaVozila.Size = new System.Drawing.Size(408, 164);
            this.lbxListaVozila.TabIndex = 0;
            // 
            // dtpDatumPocetka
            // 
            this.dtpDatumPocetka.CustomFormat = "dd/MM/yyyy";
            this.dtpDatumPocetka.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumPocetka.Location = new System.Drawing.Point(253, 249);
            this.dtpDatumPocetka.Name = "dtpDatumPocetka";
            this.dtpDatumPocetka.Size = new System.Drawing.Size(200, 22);
            this.dtpDatumPocetka.TabIndex = 1;
            // 
            // dtpDatumKraja
            // 
            this.dtpDatumKraja.CustomFormat = "dd/MM/yyyy";
            this.dtpDatumKraja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumKraja.Location = new System.Drawing.Point(253, 295);
            this.dtpDatumKraja.Name = "dtpDatumKraja";
            this.dtpDatumKraja.Size = new System.Drawing.Size(200, 22);
            this.dtpDatumKraja.TabIndex = 2;
            // 
            // lblDatumPocetka
            // 
            this.lblDatumPocetka.AutoSize = true;
            this.lblDatumPocetka.Location = new System.Drawing.Point(42, 249);
            this.lblDatumPocetka.Name = "lblDatumPocetka";
            this.lblDatumPocetka.Size = new System.Drawing.Size(180, 17);
            this.lblDatumPocetka.TabIndex = 3;
            this.lblDatumPocetka.Text = "Datum početka upravljanja:";
            // 
            // lblDatumKraja
            // 
            this.lblDatumKraja.AutoSize = true;
            this.lblDatumKraja.Location = new System.Drawing.Point(42, 295);
            this.lblDatumKraja.Name = "lblDatumKraja";
            this.lblDatumKraja.Size = new System.Drawing.Size(161, 17);
            this.lblDatumKraja.TabIndex = 4;
            this.lblDatumKraja.Text = "Datum kraja upravljanja:";
            // 
            // btnOdustani
            // 
            this.btnOdustani.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOdustani.Location = new System.Drawing.Point(264, 359);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(75, 34);
            this.btnOdustani.TabIndex = 5;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseVisualStyleBackColor = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // btnZapamti
            // 
            this.btnZapamti.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnZapamti.Location = new System.Drawing.Point(147, 359);
            this.btnZapamti.Name = "btnZapamti";
            this.btnZapamti.Size = new System.Drawing.Size(75, 34);
            this.btnZapamti.TabIndex = 6;
            this.btnZapamti.Text = "Zapamti";
            this.btnZapamti.UseVisualStyleBackColor = true;
            this.btnZapamti.Click += new System.EventHandler(this.btnZapamti_Click);
            // 
            // VozacUpravljaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 425);
            this.Controls.Add(this.btnZapamti);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.lblDatumKraja);
            this.Controls.Add(this.lblDatumPocetka);
            this.Controls.Add(this.dtpDatumKraja);
            this.Controls.Add(this.dtpDatumPocetka);
            this.Controls.Add(this.lbxListaVozila);
            this.Name = "VozacUpravljaForm";
            this.Text = "Vozila";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VozacUpravljaForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxListaVozila;
        private System.Windows.Forms.DateTimePicker dtpDatumPocetka;
        private System.Windows.Forms.DateTimePicker dtpDatumKraja;
        private System.Windows.Forms.Label lblDatumPocetka;
        private System.Windows.Forms.Label lblDatumKraja;
        private System.Windows.Forms.Button btnOdustani;
        private System.Windows.Forms.Button btnZapamti;
    }
}