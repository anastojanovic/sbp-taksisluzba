﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;


namespace SBP_faza_2.Forme
{
    public partial class TelefonMusterijeListForm : SBP_faza_2.Forme.ListForm
    {
        public TelefonMusterijeListForm()
        {
            InitializeComponent();
        }

        MusterijaBasicDTO musterija;
        TelefonMusterijeDTOManager dtoManager;
        TelefonOperationForma forma;
        

        public TelefonMusterijeListForm(MusterijaBasicDTO dto)
        {
            InitializeComponent();
            this.musterija = dto;
            lblMusterija.Text = "Musterija: " + musterija.ToString();
            dtoManager = new TelefonMusterijeDTOManager();
            forma = new TelefonOperationForma();
            base.SakrijAzurirajDugme();
            InitializeList();
        }

        protected override void InitializeList()
        {
            IEnumerable<AbstractDTO> lista = dtoManager.ReadAll((int)musterija.Id);
            getListBox().DataSource = lista;
            getListBox().Refresh();
        }

        protected override void Create()
        {

            TelefonDTO dto = null;
            forma.Dto = null;
            if (forma.ShowDialog() == DialogResult.OK)
            {
                dto = (TelefonDTO)forma.Dto;
                dto.IdMusterije = (int)musterija.Id;
                dtoManager.Create(dto);
                InitializeList();
            }

        }
        

        protected override void Delete()
        {
            TelefonDTO dto = (TelefonDTO)getSelectedItem();

            if (MessageBox.Show("Da li ste sigurni da zelite da obrisete?", "Upozorenje", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                dtoManager.Delete(dto);
                InitializeList();
            }
        }

        protected override void Read()
        {
            
            setLblMoreInfo("");
        }
    }
}
