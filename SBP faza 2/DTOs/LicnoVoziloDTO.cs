﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{

    public class LicnoVoziloShortDTO : AbstractDTO
    {
        [Required]
        public int IdVozaca { get; set; }
        public String Marka { get; set; }
        public String Tip { get; set; }

        public LicnoVoziloShortDTO(int idVozila, int idVozaca, String marka, String tip)
        {
            Id = idVozila;
            IdVozaca = idVozaca;
            Marka = marka;
            Tip = tip;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", Marka, Tip);
        }

    }

    public class LicnoVoziloLongDTO : AbstractDTO
    {
        public int IdVozaca { get; set; }
        public String Marka { get; set; }
        public String Tip { get; set; }
        public String Boja { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }

        public LicnoVoziloLongDTO(int idVozila, int idVozaca, String marka, String tip, String boja, DateTime datumOd, DateTime datumDo)
        {
            Id = idVozila;
            IdVozaca = idVozaca;
            Marka = marka;
            Tip = tip;
            Boja = boja;
            DatumOd = datumOd;
            DatumDo = datumDo;
        }

        public override string ToString()
        {
            return String.Format("Marka i tip: {0} {1}\nBoja:{2}\nVozac vozi od: {3}\nVozac vozi do: {4}", Marka, Tip, Boja, DatumOd.ToString("dd/MM/yyyy"), DatumOd.ToString("dd/MM/yyyy"));
        }
    }

    public class LicnoVoziloSuperLongDTO : LicnoVoziloLongDTO
    {
        public VozacBasicDTO Vozac { get; set; }
        public LicnoVoziloSuperLongDTO(int idVozila, int idVozaca, string marka, string tip, string boja, DateTime datumOd, DateTime datumDo, VozacBasicDTO vozac) : base(idVozila, idVozaca, marka, tip, boja, datumOd, datumDo)
        {
            this.Vozac = vozac;
        }
    }
}
