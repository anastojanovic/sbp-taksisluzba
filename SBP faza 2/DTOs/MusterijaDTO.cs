﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class MusterijaBasicDTO : AbstractDTO
    {
        public String Adresa { get; set; }

        public MusterijaBasicDTO(int id, String adresa)
        {
            Adresa = adresa;
            Id = id;
        }

        public override string ToString()
        {
            return Adresa;
        }
    }
    public class MusterijaLongDTO: AbstractDTO
    {
        public int BrojVoznji { get; set; }
        public String Adresa { get; set; }


        public MusterijaLongDTO(int id, String adresa, int brojVoznji)
        {
            Adresa = adresa;
            BrojVoznji = brojVoznji;
            Id = id;

        }

        public override string ToString()
        {
            return String.Format("Adresa musterije: {0}\nBroj voznji: {1}", Adresa, BrojVoznji);
        }

    }

    public class MusterijaSuperLongDTO : AbstractDTO
    {
        public int BrojVoznji { get; set; }
        public String Adresa { get; set; }

        public List<TelefonDTO> Telefoni { get; set; }

        public List<VoznjaBasicDTO> Voznje { get; set; }

        public MusterijaSuperLongDTO(int id, String adresa, int brojVoznji, List<TelefonDTO> listaTelefona, List<VoznjaBasicDTO> listaVoznji)
        {
            Adresa = adresa;
            BrojVoznji = brojVoznji;
            Id = id;

            Telefoni = listaTelefona;
            Voznje = listaVoznji;
        }

        public override string ToString()
        {
            return String.Format("Adresa musterije: {0}\nBroj voznji: {1}", Adresa, BrojVoznji);
        }

    }
}
