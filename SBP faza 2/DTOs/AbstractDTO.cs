﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class AbstractDTO
    {
        [Required]
        public Object Id { get; set; } 

        public AbstractDTO(Object id)
        {
            this.Id = id;
        }
        public AbstractDTO()
        {

        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is VoziloFirmeBasicDTO))
                return false;
            AbstractDTO dto = (AbstractDTO)obj;
            return this.Id == dto.Id;
        }
    }

    class AbstractDTOEqualityComparer : IEqualityComparer<AbstractDTO>
    {
        public bool Equals(AbstractDTO x, AbstractDTO y)
        {
            if (Object.ReferenceEquals(x, y))
                return true;

            if (x.GetType() != y.GetType())
                return false;

            if (x.Id.Equals(y.Id))
                return true;

            return false;
        }

        public int GetHashCode(AbstractDTO obj)
        {
            unchecked
            {
                if (obj == null)
                    return 0;
                int hashCode = obj.Id.GetHashCode();
                hashCode = (hashCode * 397) ^ obj.Id.GetHashCode();
                return hashCode;
            }
        }
    }
}
