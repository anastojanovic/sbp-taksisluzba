﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class VoznjaBasicDTO : AbstractDTO
    { 
        public String PocetnaStanica { get; set; }
        public String KrajnjaStanica { get; set; }

        public VoznjaBasicDTO(int id, String pocstan, String krajstan) : base(id)
        {
            PocetnaStanica = pocstan;
            KrajnjaStanica = krajstan;
        }

        public override string ToString()
        {
            return PocetnaStanica + " - " + KrajnjaStanica;
        }
    }


    public class VoznjaLongDTO : AbstractDTO
    {
        public virtual DateTime VremePrimanjaPoziva { get; set; }
        public int PozivniTelefon { get; set; }
        public DateTime VremePocetka { get; set; }
        public DateTime VremeKraja { get; set; }
        public String PocetnaStanica { get; set; }
        public String KrajnjaStanica { get; set; }

        public int IdAdministratora { get; set; }
        public int IdVozaca { get; set; }
        public int IdMusterije { get; set; }


        public VoznjaLongDTO(int id, String pocstan, String krajstan, DateTime pocvrem, DateTime krajvrem, DateTime vrempoz, int telpoz) : base(id)
        {
            PocetnaStanica = pocstan;
            KrajnjaStanica = krajstan;
            VremePocetka = pocvrem;
            VremeKraja = krajvrem;
            VremePrimanjaPoziva = vrempoz;
            PozivniTelefon = telpoz;
        }

        public override string ToString()
        {
            return String.Format("Pocetna stanica: {0}\nKranja stanica: {1}\nVreme pocetka:{2}\nVreme kraja:{3}\nVreme primanja poziva: {4}\nPozivni telefon: {5}",
                PocetnaStanica, KrajnjaStanica, VremePocetka, VremeKraja, VremePrimanjaPoziva, PozivniTelefon);
        }
    }

    public class VoznjaSuperLongDTO : AbstractDTO
    {
        public virtual DateTime VremePrimanjaPoziva { get; set; }
        public int PozivniTelefon { get; set; }
        public DateTime VremePocetka { get; set; }
        public DateTime VremeKraja { get; set; }
        public String PocetnaStanica { get; set; }
        public String KrajnjaStanica { get; set; }

        public AdministratorBasicDTO Administrator { get; set; }
        public VozacBasicDTO Vozac { get; set; }
        public MusterijaBasicDTO Musterija { get; set; }


        public VoznjaSuperLongDTO(int id, String pocstan, String krajstan, DateTime pocvrem, DateTime krajvrem, DateTime vrempoz, int telpoz, AdministratorBasicDTO a, VozacBasicDTO v, MusterijaBasicDTO m) : base(id)
        {
            PocetnaStanica = pocstan;
            KrajnjaStanica = krajstan;
            VremePocetka = pocvrem;
            VremeKraja = krajvrem;
            VremePrimanjaPoziva = vrempoz;
            PozivniTelefon = telpoz;
            Administrator = a;
            Vozac = v;
            Musterija = m;
        }

        public override string ToString()
        {
            return String.Format("Pocetna stanica: {0}\nKranja stanica: {1}\nVreme pocetka:{2}\nVreme kraja:{3}\nVreme primanja poziva: {4}\nPozivni telefon: {5}",
                PocetnaStanica, KrajnjaStanica, VremePocetka, VremeKraja, VremePrimanjaPoziva, PozivniTelefon);
        }
    }

}
