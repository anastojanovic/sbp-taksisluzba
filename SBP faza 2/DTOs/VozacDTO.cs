﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class VozacBasicDTO : AbstractDTO
    {
        public String LicnoIme { get; set; }
        public String Prezime { get; set; }

        public VozacBasicDTO(int id, string licnoIme, string prezime):base(id)
        {
            LicnoIme = licnoIme;
            Prezime = prezime;
        }

        public override string ToString()
        {
            return this.LicnoIme + " " + this.Prezime;
        }
    }

    public class VozacLongDTO:AbstractDTO
    {
        public String LicnoIme { get; set; }
        public String SrednjeSlovo { get; set; }
        public String Prezime { get; set; }
        public long JMBG { get; set; }
        public String Adresa { get; set; }
        public int Telefon { get; set; }
        public int BrojDozvole { get; set; }
        public String Kategorija { get; set; }

        public VozacLongDTO(int id, string licnoIme, string srednjeSlovo, string prezime, long jMBG, string adresa, int telefon, int brojDozvole, string kategorija):base(id)
        {
            LicnoIme = licnoIme;
            SrednjeSlovo = srednjeSlovo;
            Prezime = prezime;
            JMBG = jMBG;
            Adresa = adresa;
            Telefon = telefon;
            BrojDozvole = brojDozvole;
            Kategorija = kategorija;
        }

        public override string ToString()
        {
            return String.Format("Ime i prezime: {0} {1}. {2}\nJmbg: {3}\nAdresa: {4}\nTelefon:{5}\nBroj vozacke dozvole: {6}\nKategorija: {7}\n", LicnoIme, SrednjeSlovo, Prezime, JMBG, Adresa, Telefon, BrojDozvole, Kategorija);
        }
    }

    public class VozacSuperLongDTO : VozacLongDTO
    {
        public List<LicnoVoziloShortDTO> LicnaVozila { get; set; }
        public List<VoziloFirmeBasicDTO> FirminaVozila { get; set; }
        public List <VoznjaBasicDTO> Voznje { get; set; }
        public VozacSuperLongDTO(int id, string licnoIme, string srednjeSlovo, string prezime, long jMBG, string adresa, int telefon, int brojDozvole, string kategorija, List<LicnoVoziloShortDTO> listaLV, List<VoziloFirmeBasicDTO> listaVF, List<VoznjaBasicDTO> listaV) 
            : base(id, licnoIme, srednjeSlovo, prezime, jMBG, adresa, telefon, brojDozvole, kategorija)
        {
            LicnaVozila = listaLV;
            FirminaVozila = listaVF;
            Voznje = listaV;
        }
    }


}
