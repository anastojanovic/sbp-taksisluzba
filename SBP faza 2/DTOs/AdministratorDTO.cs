﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class AdministratorBasicDTO: AbstractDTO
    {

        public String LicnoIme { get; set; }
        public String Prezime { get; set; }

        public AdministratorBasicDTO(int id, String ime, String prezime): base(id)
        {
           
            this.LicnoIme = ime;
            this.Prezime = prezime;
        }

        public override string ToString()
        {
            return this.LicnoIme + " " + this.Prezime;
        }
    }

    public class AdministratorLongDTO: AbstractDTO
    {
        public String LicnoIme { get; set; }
        public String SrednjeSlovo { get; set; }
        public String Prezime { get; set; }
        public long JMBG { get; set; }
        public String Adresa { get; set; }
        public int Telefon { get; set; }
        public virtual String StrucnaSprema { get; set; }

        public AdministratorLongDTO(int idZaposlenog, string licnoIme, string srednjeSlovo, string prezime, long jMBG, string adresa, int telefon, string strucnaSprema) 
            : base(idZaposlenog)
        {
          
            LicnoIme = licnoIme;
            SrednjeSlovo = srednjeSlovo;
            Prezime = prezime;
            JMBG = jMBG;
            Adresa = adresa;
            Telefon = telefon;
            StrucnaSprema = strucnaSprema;
        }

        public override string ToString()
        {
            return String.Format("Ime i prezime: {0} {1}. {2}\nJmbg: {3}\nAdresa: {4}\nTelefon:{5}\nStrucna sprema: {6}\n", LicnoIme, SrednjeSlovo, Prezime, JMBG, Adresa, Telefon, StrucnaSprema);
        }
    }

    public class AdministratorSuperLongDTO : AdministratorLongDTO
    {
        public List<VoznjaBasicDTO> listaVoznji;
        public AdministratorSuperLongDTO(int idZaposlenog, string licnoIme, string srednjeSlovo, string prezime, long jMBG, string adresa, int telefon, string strucnaSprema, List<VoznjaBasicDTO> lista) : base(idZaposlenog, licnoIme, srednjeSlovo, prezime, jMBG, adresa, telefon, strucnaSprema)
        {
            listaVoznji = lista;
        }
    }


}
