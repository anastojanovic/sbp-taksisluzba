﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class UpravljaDTO
    {
        public int IdVozaca { get; set; }
        public int IdVozilaFirme { get; set; }

        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }

        public UpravljaDTO()
        {

        }

        public UpravljaDTO(int idVozaca, int idVozila, DateTime datumOd, DateTime datumDo)
        {
            IdVozaca = idVozaca;
            IdVozilaFirme = idVozila;
            DatumOd = datumOd;
            DatumDo = datumDo;
        }

        public override string ToString()
        {
            return String.Format("Datum od: {1} - Datum do: {2}", DatumOd.ToString("dd/MM/yyyy"), DatumDo.ToString("dd/MM/yyyy"));
        }
    }

    public class UpravljaLongDTO
    {
        public VozacBasicDTO VozacBasic { get; set; }
        public VoziloFirmeBasicDTO VoziloFirmeBasic { get; set; }

        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }

        public UpravljaLongDTO(VozacBasicDTO vozac, VoziloFirmeBasicDTO vozilo, DateTime datumOd, DateTime datumDo)
        {
            VozacBasic = vozac;
            VoziloFirmeBasic = vozilo;
            DatumOd = datumOd;
            DatumDo = datumDo;
        }

        public override string ToString()
        {
            return String.Format("Datum od: {1} - Datum do: {2}", DatumOd.ToString("dd/MM/yyyy"), DatumDo.ToString("dd/MM/yyyy"));
        }
    }

}
