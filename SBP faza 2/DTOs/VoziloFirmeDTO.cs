﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class VoziloFirmeBasicDTO: AbstractDTO
    {
        
        public String Marka { get; set; }
        public String Tip { get; set; }

        public VoziloFirmeBasicDTO(int id, string marka, string tip):base(id)
        {
            Marka = marka;
            Tip = tip;
        }

        public override string ToString()
        {
            return Marka + ", " + Tip;
        }

    }

    public class VoziloFirmeLongDTO: AbstractDTO
    {
        public int Godiste { get; set; }
        public String Marka { get; set; }
        public String Tip { get; set; }
        public String RegistarskaOznaka { get; set; }
        public DateTime IstekRegistracije { get; set; }

        public VoziloFirmeLongDTO(int id, int godiste, string marka, string tip, string registarskaOznaka, DateTime istekRegistracije) : base(id)
        {
            Godiste = godiste;
            Marka = marka;
            Tip = tip;
            RegistarskaOznaka = registarskaOznaka;
            IstekRegistracije = istekRegistracije;
        }

        public override string ToString()
        {
            return String.Format("Automobil: {0}\nTip: {1}\nGodiste:{2}\nRegistarska oznaka: {3}\nRegistrovan do: {4}", Marka, Tip, Godiste, RegistarskaOznaka, IstekRegistracije.ToString("dd/MM/yyyy"));
        }
    }

    public class VoziloFirmeSuperLongDTO : VoziloFirmeLongDTO
    {
        public List<VozacBasicDTO> Vozaci;

        public VoziloFirmeSuperLongDTO(int id, int godiste, string marka, string tip, string registarskaOznaka, DateTime istekRegistracije, List<VozacBasicDTO> lista) 
            : base(id, godiste, marka, tip, registarskaOznaka, istekRegistracije)
        {
            Vozaci = lista;
        }
    }
}
