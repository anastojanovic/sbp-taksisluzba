﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOs
{
    public class TelefonDTO
    {
        public int Telefon { get; set; }
        public int IdMusterije { get; set; }
        
        public TelefonDTO(int idM, int tel)
        {
            Telefon = tel;
            IdMusterije = idM;
        }

        public override string ToString()
        {
            return Telefon.ToString();
        }
    }
}
