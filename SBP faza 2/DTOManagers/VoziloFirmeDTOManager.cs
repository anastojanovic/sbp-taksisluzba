﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBP_faza_2.DTOs;
using NHibernate;
using NHibernate.Linq;
using SBP_faza_2.Entiteti;
namespace SBP_faza_2.DTOManagers
{
    public class VoziloFirmeDTOManager 
    {
        public bool Create(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                VoziloFirmeLongDTO vdto = (VoziloFirmeLongDTO)o;
                VoziloFirme v = new VoziloFirme();
                v.Marka = vdto.Marka;
                v.Tip = vdto.Tip;
                v.Godiste = vdto.Godiste;
                v.RegistarskaOznaka = vdto.RegistarskaOznaka;
                v.IstekRegistracije = vdto.IstekRegistracije;

                s.Save(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool Delete(object id)
        {
            try
            {
                int idZaBrisanje = (int)id;
                ISession s = DataLayer.GetSession();
                VoziloFirme v = s.Load<VoziloFirme>(idZaBrisanje);
                s.Delete(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }



        public VoziloFirmeSuperLongDTO Read(object id)
        {
            try {
                ISession s = DataLayer.GetSession();
                int idVozila = (int)id;
                VoziloFirme vozilo = (from v in s.Query<VoziloFirme>()
                                      where v.IdVozila == idVozila
                                      select v).FirstOrDefault();
                if (vozilo == null) return null;           
                List<VozacBasicDTO> vozaci = null;
                if(vozilo.Vozaci != null)
                {
                    vozaci = new List<VozacBasicDTO>();
                    foreach(Vozac v in vozilo.Vozaci)
                    {
                        vozaci.Add(new VozacBasicDTO(v.IdZaposlenog, v.LicnoIme, v.Prezime));
                    }
                }
                s.Close();
                return new VoziloFirmeSuperLongDTO(vozilo.IdVozila, vozilo.Godiste, vozilo.Marka, vozilo.Tip, vozilo.RegistarskaOznaka, vozilo.IstekRegistracije, vozaci);

            }
            catch(Exception e)
            {
                return null;
            }
        }
        
        public IEnumerable<AbstractDTO> ReadAll()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<VoziloFirme> vozila = from v in s.Query<VoziloFirme>() select v;

                List<VoziloFirmeBasicDTO> lista = new List<VoziloFirmeBasicDTO>();
                foreach (VoziloFirme v in vozila)
                {
                    lista.Add(new VoziloFirmeBasicDTO(v.IdVozila, v.Marka, v.Tip));
                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }
        }


        public IEnumerable<AbstractDTO> ReadAll(int idVozaca)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Vozac vozac = (from v in s.Query<Vozac>() where v.IdZaposlenog == idVozaca select v).FirstOrDefault();
                List<VoziloFirmeBasicDTO> vozila = new List<VoziloFirmeBasicDTO>();

                foreach (VoziloFirme vf in vozac.VozilaFirme)
                {
                    vozila.Add(new VoziloFirmeBasicDTO(vf.IdVozila, vf.Marka, vf.Tip));
                }

                s.Close();
                return vozila;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public bool Update(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int id = (int)o.Id;
                VoziloFirmeLongDTO vdto = (VoziloFirmeLongDTO)o;
                VoziloFirme v = s.Load<VoziloFirme>(id);
                v.Marka = vdto.Marka;
                v.Tip = vdto.Tip;
                v.Godiste = vdto.Godiste;
                v.RegistarskaOznaka = vdto.RegistarskaOznaka;
                v.IstekRegistracije = vdto.IstekRegistracije;

                s.Update(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
