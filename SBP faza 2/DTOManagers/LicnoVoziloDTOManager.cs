﻿using NHibernate;
using SBP_faza_2.DTOs;
using SBP_faza_2.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOManagers
{
    public class LicnoVoziloDTOManager
    {

        public bool Create(LicnoVoziloLongDTO dto)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Vozac vozac = s.Load<Vozac>(dto.IdVozaca);
                LicnoVozilo v = new LicnoVozilo()
                {
                    Vozac = vozac,
                    Boja = dto.Boja,
                    Tip = dto.Tip,
                    Marka = dto.Marka,
                    DatumOd = dto.DatumOd,
                    DatumDo = dto.DatumDo

                };
                s.Save(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public bool Delete(int idVozila)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                LicnoVozilo vozilo = s.Load<LicnoVozilo>(idVozila);
                s.Delete(vozilo);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public LicnoVoziloSuperLongDTO Read(int idVozila)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                LicnoVozilo vozilo = s.Load<LicnoVozilo>(idVozila);
                if (vozilo == null) return null;
                VozacBasicDTO vozac = null;
                if(vozilo.Vozac != null)
                    vozac = new VozacBasicDTO(vozilo.Vozac.IdZaposlenog, vozilo.Vozac.LicnoIme, vozilo.Vozac.Prezime);
                LicnoVoziloSuperLongDTO dto = new LicnoVoziloSuperLongDTO(vozilo.IdVozila, vozilo.Vozac.IdZaposlenog, vozilo.Marka, vozilo.Tip, vozilo.Boja, vozilo.DatumOd, vozilo.DatumDo, vozac);
                s.Close();

                return dto;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public bool Update(LicnoVoziloLongDTO dto)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                LicnoVozilo v = s.Load<LicnoVozilo>(dto.Id);
                v.Boja = dto.Boja;
                v.Tip = dto.Tip;
                v.Marka = dto.Marka;
                v.DatumOd = dto.DatumOd;
                v.DatumDo = dto.DatumDo;

                s.Update(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public IEnumerable<AbstractDTO> ReadAll(int idVozaca)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<LicnoVozilo> vozila = from a in s.Query<LicnoVozilo>()
                                                  where a.Vozac.IdZaposlenog == idVozaca
                                                  select a;

                List<LicnoVoziloShortDTO> lista = new List<LicnoVoziloShortDTO>();
                foreach (LicnoVozilo v in vozila)
                {
                    lista.Add(new LicnoVoziloShortDTO(v.IdVozila, v.Vozac.IdZaposlenog, v.Marka, v.Tip));
                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
