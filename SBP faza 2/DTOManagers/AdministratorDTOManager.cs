﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using SBP_faza_2.DTOs;
using SBP_faza_2.Entiteti;

namespace SBP_faza_2.DTOManagers
{
    public class AdministratorDTOManager
    { 
        public bool Create(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                AdministratorLongDTO adto = (AdministratorLongDTO)o;
                Administrator a = new Administrator();
                a.LicnoIme = adto.LicnoIme;
                a.SrednjeSlovo = adto.SrednjeSlovo;
                a.Prezime = adto.Prezime;
                a.JMBG = adto.JMBG;
                a.Adresa = adto.Adresa;
                a.Telefon = adto.Telefon;
                a.StrucnaSprema = adto.StrucnaSprema;
                s.Save(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public bool Delete(object id)
        {
           try
           {
                int idZaBrisanje = (int)id;
                ISession s = DataLayer.GetSession();
                Administrator a = s.Load<Administrator>(idZaBrisanje);
                s.Delete(a);
                s.Flush();
                s.Close();
                return true;
            }
           catch(Exception e)
           {
                return false;
           }

        }

        public AdministratorSuperLongDTO Read(object id)
        {
            try
            {

                ISession s = DataLayer.GetSession();
                int idAdm = (int)id;
                Administrator admin = (from a in s.Query<Administrator>()
                                       where a.IdZaposlenog == idAdm
                                       select a).FirstOrDefault();
                if (admin == null) return null;
                List<VoznjaBasicDTO> lista = null;
                if (admin.Voznje != null)
                {
                    lista = new List<VoznjaBasicDTO>();
                    foreach(Voznja v in admin.Voznje)
                    {
                        lista.Add(new VoznjaBasicDTO(v.IdVoznje, v.PocetnaStanica, v.KrajnjaStanica));
                    }

                }

                s.Close();
                return new AdministratorSuperLongDTO(admin.IdZaposlenog, admin.LicnoIme, admin.SrednjeSlovo, admin.Prezime, admin.JMBG, admin.Adresa, admin.Telefon, admin.StrucnaSprema, lista);
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public AbstractDTO ReadShort(object id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int idAdm = (int)id;
                Administrator admin = (from a in s.Query<Administrator>()
                                       where a.IdZaposlenog == idAdm
                                       select a).FirstOrDefault();
                s.Close();
                return new AdministratorBasicDTO(admin.IdZaposlenog, admin.LicnoIme, admin.Prezime);
            }
            catch(Exception e)
            {
                return null;
            }

        }

        public IEnumerable<AbstractDTO> ReadAll()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Administrator> admini = from a in s.Query<Administrator>() select a;

                List<AdministratorBasicDTO> lista = new List<AdministratorBasicDTO>();
                foreach (Administrator admin in admini)
                {
                    lista.Add(new AdministratorBasicDTO(admin.IdZaposlenog, admin.LicnoIme, admin.Prezime));
                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }

        }

        public bool Update(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int id = (int)o.Id;
                AdministratorLongDTO adto = (AdministratorLongDTO)o;
                Administrator a = s.Load<Administrator>(id);
                a.LicnoIme = adto.LicnoIme;
                a.SrednjeSlovo = adto.SrednjeSlovo;
                a.Prezime = adto.Prezime;
                a.JMBG = adto.JMBG;
                a.Adresa = adto.Adresa;
                a.Telefon = adto.Telefon;
                a.StrucnaSprema = adto.StrucnaSprema;
                s.Update(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }
    }
}
