﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using SBP_faza_2.DTOs;

namespace SBP_faza_2.DTOManagers
{
    public interface IAbstractDTOManager
    {
        AbstractDTO Read(Object id);
        bool Create(AbstractDTO o);
        bool Update(AbstractDTO o);
        bool Delete(Object id);
        IEnumerable<AbstractDTO> ReadAll();

    }
}
