﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBP_faza_2.DTOs;
using NHibernate;
using SBP_faza_2.Entiteti;

namespace SBP_faza_2.DTOManagers
{
    public class MusterijaDTOManager
    {
        public bool Create(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                MusterijaLongDTO adto = (MusterijaLongDTO)o;
                Musterija a = new Musterija();
                a.Adresa = adto.Adresa;
                s.Save(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public bool Delete(object id)
        {
            try
            {
                int idZaBrisanje = (int)id;
                ISession s = DataLayer.GetSession();
                Musterija a = s.Load<Musterija>(idZaBrisanje);
                s.Delete(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public MusterijaSuperLongDTO Read(object id)
        {
            //try
            {
                ISession s = DataLayer.GetSession();
                int idM = (int)id;
                Musterija musterija = (from a in s.Query<Musterija>()
                                       where a.IdMusterije == idM
                                       select a).FirstOrDefault();
                if (musterija == null) return null;
                List<TelefonDTO> listaTelefona = new List<TelefonDTO>();
                List<VoznjaBasicDTO> listaVoznji = new List<VoznjaBasicDTO>();
                foreach(TelefonMusterije t in musterija.Telefoni)
                {
                    listaTelefona.Add(new TelefonDTO(t.Musterija.IdMusterije, t.Telefon));
                }
                foreach(Voznja v in musterija.Voznje)
                {
                    listaVoznji.Add(new VoznjaBasicDTO(v.IdVoznje, v.PocetnaStanica, v.KrajnjaStanica));
                }
                MusterijaSuperLongDTO dto = new MusterijaSuperLongDTO(musterija.IdMusterije, musterija.Adresa ,musterija.BrojVoznji, listaTelefona, listaVoznji);
                
                s.Close();
                return dto;
            }
            //catch(Exception e)
            {
                return null;
            }
        }

        public IEnumerable<AbstractDTO> ReadAll()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Musterija> musterije = from a in s.Query<Musterija>() select a;

                List<MusterijaBasicDTO> lista = new List<MusterijaBasicDTO>();
                foreach (Musterija m in musterije)
                {
                    lista.Add(new MusterijaBasicDTO(m.IdMusterije, m.Adresa));
                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }

        }

        public bool Update(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int id = (int)o.Id;
                MusterijaBasicDTO dto = (MusterijaBasicDTO)o;
                Musterija a = s.Load<Musterija>(id);
                a.Adresa = dto.Adresa;
                s.Update(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }

        public AbstractDTO ReadShort(object id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int idM = (int)id;
                Musterija m = (from a in s.Query<Musterija>()
                               where a.IdMusterije == idM
                               select a).FirstOrDefault();
                s.Close();
                return new MusterijaBasicDTO(m.IdMusterije, m.Adresa);
            }
            catch(Exception e)
            {
                return null;
            }

        }

    }
}
