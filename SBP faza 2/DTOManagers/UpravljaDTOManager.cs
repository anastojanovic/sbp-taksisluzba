﻿using NHibernate;
using SBP_faza_2.DTOs;
using SBP_faza_2.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.DTOManagers
{
    public class UpravljaDTOManager
    {
        public IEnumerable<UpravljaDTO> ReadAll()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Upravlja> listaUpravlja = from v in s.Query<Upravlja>() select v;

                List<UpravljaDTO> lista = new List<UpravljaDTO>();
                foreach (Upravlja u in listaUpravlja)
                {
                    lista.Add(new UpravljaDTO(u.Id.UpravljaVozac.IdZaposlenog, u.Id.UpravljaVoziloFirme.IdVozila, u.DatumOd, u.DatumDo));
                }
                s.Close();
                return lista;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public UpravljaLongDTO Read(int idVozila, int idVozaca)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Upravlja upravlja = (from u in s.Query<Upravlja>()
                                      where u.Id.UpravljaVoziloFirme.IdVozila == idVozila && u.Id.UpravljaVozac.IdZaposlenog == idVozaca
                                      select u).FirstOrDefault();
                UpravljaLongDTO dto = null;
                if(upravlja != null)
                {
                    VozacBasicDTO vozac = new VozacBasicDTO(upravlja.Id.UpravljaVozac.IdZaposlenog, upravlja.Id.UpravljaVozac.LicnoIme, upravlja.Id.UpravljaVozac.Prezime);
                    VoziloFirmeBasicDTO vozilo = new VoziloFirmeBasicDTO(upravlja.Id.UpravljaVoziloFirme.IdVozila, upravlja.Id.UpravljaVoziloFirme.Marka, upravlja.Id.UpravljaVoziloFirme.Tip);
                    dto = new UpravljaLongDTO(vozac, vozilo, upravlja.DatumOd, upravlja.DatumDo);
                }
                s.Close();
                return dto;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool Update(UpravljaDTO dto)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Upravlja upravlja = (from u in s.Query<Upravlja>()
                                     where u.Id.UpravljaVoziloFirme.IdVozila == dto.IdVozilaFirme && u.Id.UpravljaVozac.IdZaposlenog == dto.IdVozaca
                                     select u).FirstOrDefault();

                upravlja.DatumOd = dto.DatumOd;
                upravlja.DatumDo = dto.DatumDo;
                s.Update(upravlja);
                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Delete(object idVozaca, object idVozila)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int vozacZaBrisanje = (int)idVozaca;
                int voziloZaBrisanje = (int)idVozila;

                Upravlja upravlja = (from u in s.Query<Upravlja>()
                                     where u.Id.UpravljaVozac.IdZaposlenog == vozacZaBrisanje &&
                                     u.Id.UpravljaVoziloFirme.IdVozila == voziloZaBrisanje
                                     select u).FirstOrDefault();
                s.Delete(upravlja);
                s.Flush();
                s.Close();

                return true;
            }
            catch(Exception e)
            {
                return false;
            }


        }

        public bool Create(UpravljaDTO dto)
        {
            try
            {            
                ISession s = DataLayer.GetSession();
                Vozac vozac = (from v in s.Query<Vozac>() where v.IdZaposlenog == dto.IdVozaca select v).FirstOrDefault();
                VoziloFirme vozilo = (from vf in s.Query<VoziloFirme>() where vf.IdVozila == dto.IdVozilaFirme select vf).FirstOrDefault();
                UpravljaId uId = new UpravljaId()
                {
                    UpravljaVozac = vozac,
                    UpravljaVoziloFirme = vozilo
                };
                Upravlja u = new Upravlja()
                {
                    DatumOd = dto.DatumOd,
                    DatumDo = dto.DatumDo,
                    Id = uId
                };

                s.Save(u);
                s.Flush();
                s.Close();
                return true;         

            }
            catch(Exception e)
            {
                return false;
            }
           
        }
    }
}
