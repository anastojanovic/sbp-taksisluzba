﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBP_faza_2.DTOs;
using NHibernate;
using SBP_faza_2.Entiteti;

namespace SBP_faza_2.DTOManagers
{
    public class VoznjaDTOManager : IAbstractDTOManager
    {
        public bool Create(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                VoznjaLongDTO v = (VoznjaLongDTO)o;
                Musterija m = s.Load<Musterija>(v.IdMusterije);
                Vozac vozac = s.Load<Vozac>(v.IdVozaca);
                ISession s1 = DataLayer.GetSession();
                Administrator a = s1.Load<Administrator>(v.IdAdministratora);

                Voznja voznja = new Voznja()
                {
                    VremePocetka = v.VremePocetka,
                    VremeKraja = v.VremeKraja,
                    PocetnaStanica = v.PocetnaStanica,
                    KrajnjaStanica = v.KrajnjaStanica,
                    PozivniTelefon = v.PozivniTelefon,
                    VremePrimanjaPoziva = v.VremePrimanjaPoziva,
                    Musterija = m,
                    Vozac = vozac,
                    Administrator = a

                };

                s.Save(voznja);
                s.Flush();
                s.Close();
                s1.Close();

            }catch(Exception e)
            {
                return false;
            }
            return true;
        }

        public bool Delete(object id)
        {
            try
            {
                int idZaBrisanje = (int)id;
                ISession s = DataLayer.GetSession();
                Voznja a = s.Load<Voznja>(idZaBrisanje);
                s.Delete(a);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public AbstractDTO Read(object id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                ISession s2 = DataLayer.GetSession();
                ISession s3 = DataLayer.GetSession();
                int idVoznje = (int)id;
                Voznja voznja = (from a in s.Query<Voznja>()
                                 where a.IdVoznje == idVoznje
                                 select a).FirstOrDefault();
                if (voznja == null) return null;
                AdministratorBasicDTO admin = null;
                VozacBasicDTO vozac = null;
                MusterijaBasicDTO musterija = null;
                if (voznja.Administrator != null)
                {
                    /*   Administrator a = (from ad in s2.Query<Administrator>() where ad.IdZaposlenog == voznja.Administrator.IdZaposlenog select ad).FirstOrDefault();
                       admin = new AdministratorBasicDTO(a.IdZaposlenog, a.LicnoIme, a.Prezime);*/
                    admin = new AdministratorBasicDTO(voznja.Administrator.IdZaposlenog, voznja.Administrator.LicnoIme, voznja.Administrator.Prezime);
                }
                if (voznja.Vozac != null)
                {
                    Vozac v = (from voz in s3.Query<Vozac>() where voz.IdZaposlenog == voznja.Vozac.IdZaposlenog select voz).FirstOrDefault();
                    vozac = new VozacBasicDTO(voznja.Vozac.IdZaposlenog, voznja.Vozac.LicnoIme, voznja.Vozac.Prezime);
                }
                    if (voznja.Musterija != null)
                    musterija = new MusterijaBasicDTO(voznja.Musterija.IdMusterije, voznja.Musterija.Adresa);
                s.Close();
                return new VoznjaSuperLongDTO(voznja.IdVoznje, voznja.PocetnaStanica, voznja.KrajnjaStanica, voznja.VremePocetka, voznja.VremeKraja, voznja.VremePrimanjaPoziva, voznja.PozivniTelefon, admin, vozac, musterija);
 
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public IEnumerable<AbstractDTO> ReadAll()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Voznja> voznje = from a in s.Query<Voznja>() select a;

                List<VoznjaBasicDTO> lista = new List<VoznjaBasicDTO>();
                foreach (Voznja v in voznje)
                {
                    lista.Add(new VoznjaBasicDTO(v.IdVoznje, v.PocetnaStanica, v.KrajnjaStanica));
                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }

        }

        public bool Update(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                int id = (int)o.Id;
                VoznjaLongDTO vdto = (VoznjaLongDTO)o;
                Voznja v = s.Load<Voznja>(id);
                Musterija m = s.Load<Musterija>(vdto.IdMusterije);
                Vozac vozac = s.Load<Vozac>(vdto.IdVozaca);
                ISession s1 = DataLayer.GetSession();
                Administrator a = s.Load<Administrator>(vdto.IdAdministratora);

                v.PocetnaStanica = vdto.PocetnaStanica;
                v.KrajnjaStanica = vdto.KrajnjaStanica;
                v.VremePrimanjaPoziva = vdto.VremePrimanjaPoziva;
                v.VremePocetka = vdto.VremePocetka;
                v.VremeKraja = vdto.VremeKraja;
                v.PozivniTelefon = vdto.PozivniTelefon;
                v.Administrator = a;
                v.Vozac = vozac;
                v.Musterija = m;
                s.Update(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }
    }
}
