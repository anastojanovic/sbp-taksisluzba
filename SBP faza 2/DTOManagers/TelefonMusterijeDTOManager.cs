﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBP_faza_2.DTOs;
using SBP_faza_2.Entiteti;
using NHibernate;

namespace SBP_faza_2.DTOManagers
{
    public class TelefonMusterijeDTOManager
    {
        public bool Create(TelefonDTO dto)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Musterija m = s.Get<Musterija>(dto.IdMusterije);
                TelefonMusterije t = new TelefonMusterije()
                {
                    Telefon = dto.Telefon,
                    Musterija = m
                };
                s.Save(t);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }
        public TelefonDTO Read(int tel)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                TelefonMusterije telefon = (from a in s.Query<TelefonMusterije>()
                                            where a.Telefon == tel
                                            select a).FirstOrDefault();
                return new TelefonDTO(telefon.Musterija.IdMusterije, telefon.Telefon);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool Delete(TelefonDTO dto)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Musterija m = s.Get <Musterija> (dto.IdMusterije);
                TelefonMusterije tel = (from t in s.Query<TelefonMusterije>()
                                        where t.Musterija == m && t.Telefon == dto.Telefon
                                        select t).FirstOrDefault();
                s.Delete(tel);
                s.Flush();
                s.Close();
                return true;

            }
            catch(Exception e)
            {
                return false;
            }
        }

        public IEnumerable<TelefonDTO> ReadAll(int idMusterije)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Musterija m = s.Get<Musterija>(idMusterije);

                IEnumerable<TelefonMusterije> telefoni = from a in s.Query<TelefonMusterije>()
                                                         where a.Musterija== m
                                                         select a;

                List<TelefonDTO> lista = new List<TelefonDTO>();
                foreach (TelefonMusterije t in telefoni)
                {
                    lista.Add(new TelefonDTO(t.Musterija.IdMusterije, t.Telefon));

                }
                s.Close();
                return lista;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        
    }
}
