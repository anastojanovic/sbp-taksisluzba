﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBP_faza_2.DTOs;
using NHibernate;
using NHibernate.Linq;
using SBP_faza_2.Entiteti;

namespace SBP_faza_2.DTOManagers
{
    public class VozacDTOManager 
    {
        public bool Create(AbstractDTO o)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                VozacLongDTO vdto = (VozacLongDTO)o;
                Vozac v = new Vozac();
                v.LicnoIme = vdto.LicnoIme;
                v.SrednjeSlovo = vdto.SrednjeSlovo;
                v.Prezime = vdto.Prezime;
                v.JMBG = vdto.JMBG;
                v.Adresa = vdto.Adresa;
                v.Telefon = vdto.Telefon;
                v.BrojDozvole = vdto.BrojDozvole;
                v.Kategorija = vdto.Kategorija;
                s.Save(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public AbstractDTO ReadShort(object id)
        {
            ISession s = DataLayer.GetSession();
            int idVoz = (int)id;
            Vozac vozac = (from a in s.Query<Vozac>()
                                   where a.IdZaposlenog == idVoz
                                   select a).FirstOrDefault();
            s.Close();
            return new VozacBasicDTO(vozac.IdZaposlenog, vozac.LicnoIme, vozac.Prezime);

        }

        public bool Delete(object id)
        {
            try
            {
                int idZaBrisanje = (int)id;
                ISession s = DataLayer.GetSession();
                Vozac v = s.Load<Vozac>(idZaBrisanje);
                s.Delete(v);
                s.Flush();
                s.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public VozacSuperLongDTO Read(object id)
        {
            ISession s = DataLayer.GetSession();
            int idVozaca = (int)id;
            Vozac vozac = (from v in s.Query<Vozac>()
                                   where v.IdZaposlenog == idVozaca
                                   select v).FirstOrDefault();
            if (vozac == null) return null;
            List<VoznjaBasicDTO> listaV = null;
            List<VoziloFirmeBasicDTO> listaVF = null;
            List<LicnoVoziloShortDTO> listaLV = null;
            if (vozac.Voznje != null)
            {
                listaV = new List<VoznjaBasicDTO>();
                foreach (Voznja v in vozac.Voznje)
                {
                    listaV.Add(new VoznjaBasicDTO(v.IdVoznje, v.PocetnaStanica, v.KrajnjaStanica));
                }

            }
            if(vozac.VozilaFirme != null)
            {
                listaVF = new List<VoziloFirmeBasicDTO>();
                foreach(VoziloFirme v in vozac.VozilaFirme)
                {
                    listaVF.Add(new VoziloFirmeBasicDTO(v.IdVozila, v.Marka, v.Tip));
                }
            }
            if(vozac.LicnaVozila != null)
            {
                listaLV = new List<LicnoVoziloShortDTO>();
                foreach(LicnoVozilo l in vozac.LicnaVozila)
                {
                    listaLV.Add(new LicnoVoziloShortDTO(l.IdVozila, vozac.IdZaposlenog, l.Marka, l.Tip));
                }
            }
            s.Close();
            return new VozacSuperLongDTO(vozac.IdZaposlenog, vozac.LicnoIme, vozac.SrednjeSlovo, vozac.Prezime, vozac.JMBG, vozac.Adresa, vozac.Telefon, vozac.BrojDozvole, vozac.Kategorija, listaLV, listaVF, listaV);
        }

        public IEnumerable<AbstractDTO> ReadAll()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Vozac> vozaci = from v in s.Query<Vozac>() select v;

            List<VozacBasicDTO> lista = new List<VozacBasicDTO>();
            foreach (Vozac v in vozaci)
            {
                lista.Add(new VozacBasicDTO(v.IdZaposlenog, v.LicnoIme, v.Prezime));
            }
            s.Close();
            return lista;
        }

        public bool Update(AbstractDTO o)
        {
            ISession s = DataLayer.GetSession();
            int id = (int)o.Id;
            VozacLongDTO vdto = (VozacLongDTO)o;
            Vozac v = s.Load<Vozac>(id);
            v.LicnoIme = vdto.LicnoIme;
            v.SrednjeSlovo = vdto.SrednjeSlovo;
            v.Prezime = vdto.Prezime;
            v.JMBG = vdto.JMBG;
            v.Adresa = vdto.Adresa;
            v.Telefon = vdto.Telefon;
            v.Kategorija = vdto.Kategorija;
            v.BrojDozvole = vdto.BrojDozvole;
            s.Update(v);
            s.Flush();
            s.Close();
            return true;
        }
    }
}
