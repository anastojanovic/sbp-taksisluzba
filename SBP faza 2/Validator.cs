﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;   //Za regex

namespace ValidatorPodataka

{
    public static class Validator
    {
        //Radi validaciju imena ili prezimena - Prvo veliko slovo, ostala mala
        public static bool ValidirajIme(String s)
        {
            if (s == null)
                throw new ArgumentException("Ime ne sme biti prazno!");

            Regex rgxIme = new Regex("^[A-ZŠĐČĆŽ]{1}[a-zšđčćž]{0,20}$");
            if (rgxIme.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Ime se mora sastojati od slova i pocinjati prvim velikim slovom. Ne sme biti duze od 20 karaktera.");
        }

        public static bool ValidirajGenericString(String zavalidaciju, String imePolja)
        {
            if (zavalidaciju == null || zavalidaciju=="")
                throw new ArgumentException( imePolja + " polje ne sme biti prazno!");

            Regex rgxIme = new Regex(@"^[A-ZŠĐČĆŽa-zšđčćž\s]{0,20}$");
            if (rgxIme.IsMatch(zavalidaciju))
            {
                return true;
            }
            throw new ArgumentException("Greska pri unosu polja " + imePolja + ". Naziv ne sme sadrzati brojeve i mora imati maksimalno 20 karaktera.");
        }

        //Validacija srednjeg slova
        public static bool ValidirajSlovo(String s)
        {
            if (s == null)
                throw new ArgumentException("Slovo ne sme biti prazno!");

            Regex rgxIme = new Regex("^[A-ZŠĐČĆŽ]{1}$");
            if (rgxIme.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Slovo mora biti veliko. Unosi se samo jedno slovo.");
        }

        public static bool isAdressKey(char c)
        {
            Regex rgxAdresa = new Regex(@"[A-Za-zŠĐČĆŽšđčćž0-9\s\\\/-]");
            if (rgxAdresa.IsMatch(c.ToString()))
            {
                return true;
            }

            return false;
        }

        //Radi validaciju adrese
        public static bool ValidirajAdresu(String s)
        {
            if(s == null || s == "" )
                throw new ArgumentException("Adresa ne sme biti prazna!");

            Regex rgxAdresa = new Regex(@"^[A-ZŠĐĆŽ]{1}[A-Za-zŠĐČĆŽšđčćž0-9\s\\\/-]{0,100}[A-Za-zŠĐČĆŽšđčćž0-9]{1}$");
            if (rgxAdresa.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException(@"Adresa mora pocinjati prvim velikim slovom. Dozvoljene su cifre, brojevi, blanko, i karakteri '\' i '-'. Ne sme biti duza od 100 karaktera. Ne sme se zavrsavati specijalnim karakterima.");
        }

        public static bool ValidirajTelefon(String s)
        {
            if (s == null || s == "")
                throw new ArgumentException("Telefon ne sme biti prazan!");

            Regex rgxAdresa = new Regex(@"^[0-9]{4,16}$");
            if (rgxAdresa.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException(@"Telefon se sastoji samo od cifara. Mora ih biti izmedju 4 i 15.");

        }

        public static bool ValidirajGenericBroj(String s, String naziv)
        {
            if (s == null || s=="")
                throw new ArgumentException(naziv + " ne sme biti prazan!");

            Regex rgxAdresa = new Regex(@"^[0-9]{1,13}$");
            if (rgxAdresa.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException(naziv + @" se sastoji samo od cifara. Ne sme biti prazan, ne sme postojati vise od 13 cifara.");

        }

        public static bool ValidirajDateString(String s)
        {
            if (s == null || s == "")
                throw new ArgumentException("Datum ne sme biti prazan!");

            s = s.ToString();
            Regex rgxDatum = new Regex(@"^[0-9]{2}[.][0-9]{2}[.][0-9]{4}[.]$");
            if (rgxDatum.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Datum mora biti napisan u formatu dd.MM.yyyy.");
        }

        //Proverava da li je datum u odgovarajucem rasponu
        public static bool ValidirajDatum(DateTime dtm)
        {
            if (dtm == null || DateTime.Compare(dtm, new DateTime(1753, 1, 1)) < 0)
            {
                throw new ArgumentException("Neki datumi iz baze nisu uspesno prikazani.");
            }
            return true;
        }

        public static bool ValidirajDatum(DateTime pocetak, DateTime kraj)
        {
            if (pocetak == null || kraj == null ||  DateTime.Compare(kraj, pocetak) < 0)
            {
                throw new ArgumentException("Datumi moraju da idu u odgovrajucem redosledu. Vreme pocetka nekog dogadjaja mora biti manje od vremena kraja nekog dogadjaja.");
            }
            return true;
        }



        public static bool ValidirajKategoriju(String s)
        {
            if (s == null || s == "")
                throw new ArgumentException("Kategorija ne sme biti prazna!");

            Regex rgxIme = new Regex(@"^[A-ZŠĐČĆŽ]{1,2}[0-9]{0,1}$");
            if (rgxIme.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Slovo mora biti veliko. Unosi se u formatu S, Sb, SS ili SSb, gde je S veliko slovo a b broj.");
        }

        public static bool ValidirajGodiste(String s)
        {
            if (s == null || s == "")
                throw new ArgumentException("Godiste ne sme biti prazno!");

            s = s.ToString();
            Regex rgxDatum = new Regex(@"^\d{4}$");
            if (rgxDatum.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Godiste mora biti napisano u formatu cccc, gde je c cifra.");
        }

        public static bool ValidirajRegistraciju(String s)
        {
            if (s == null || s == "")
                throw new ArgumentException("Registracija ne sme biti prazna!");

            s = s.ToString();
            Regex rgx = new Regex(@"^[A-ZŠĐĆŽ]{2}[\s]{1}[\d]{3}[-]{1}[A-ZŠĐĆŽ]{2}$");
            if (rgx.IsMatch(s))
            {
                return true;
            }
            throw new ArgumentException("Registracija mora da bude u formatu SS ccc-SS, gde je S veliko slovo a c cifra.");
        }

    }
}
