﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class LicnoVoziloId
    {
        public virtual Vozac Vozac { get; set; }
        public virtual int IdVozila { get; protected set; }

        public LicnoVoziloId()
        {

        }

        public LicnoVoziloId(int id, Vozac vozac)
        {
            IdVozila = id;
            Vozac = vozac;
        }
        
        public override bool Equals(object obj)
        {
            if (Object.ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != typeof(LicnoVoziloId))
                return false;

            LicnoVoziloId recievedObject = (LicnoVoziloId)obj;

            if ((Vozac.IdZaposlenog == recievedObject.Vozac.IdZaposlenog) &&
                ( IdVozila == recievedObject.IdVozila))
            {
                return true;
            }

            return false;

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
