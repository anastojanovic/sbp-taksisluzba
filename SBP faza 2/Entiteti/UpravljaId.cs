﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class UpravljaId
    {
        public virtual VoziloFirme UpravljaVoziloFirme { get; set; }
        public virtual Vozac UpravljaVozac { get; set; }

        public UpravljaId()
        {
            UpravljaVoziloFirme = new VoziloFirme();
            UpravljaVozac = new Vozac();
        }

        public override bool Equals(object obj)
        {
            if (Object.ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != typeof(UpravljaId))
                return false;

            UpravljaId recievedObject = (UpravljaId)obj;

            if ((UpravljaVozac.IdZaposlenog == recievedObject.UpravljaVozac.IdZaposlenog) &&
                (UpravljaVoziloFirme.IdVozila == recievedObject.UpravljaVoziloFirme.IdVozila))
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
