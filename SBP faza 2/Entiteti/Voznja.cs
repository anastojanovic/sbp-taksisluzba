﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class Voznja
    {
        public virtual int IdVoznje { get; protected set; }
        public virtual DateTime VremePrimanjaPoziva { get; set; }
        public virtual int PozivniTelefon { get; set; }
        public virtual DateTime VremePocetka { get; set; }
        public virtual DateTime VremeKraja { get; set; }
        public virtual String PocetnaStanica { get; set; }
        public virtual String KrajnjaStanica { get; set; }

        public virtual Administrator Administrator { get; set; }
        public virtual Vozac Vozac { get; set; }
        public virtual Musterija Musterija { get; set; }

        public Voznja()
        {
            Administrator = new Administrator();
            Vozac = new Vozac();
            Musterija = new Musterija();
          
        }

        public override string ToString()
        {
            return String.Format("Voznja broj: {0}, Vreme poziva: {1}, Vreme pocetka: {2}, Vreme kraja {3}, Pocetna stanica: {4}, Krajnja stanica: {5}\n",
                IdVoznje.ToString(), VremePrimanjaPoziva.ToString(), PozivniTelefon.ToString(), VremePocetka.ToString(), VremeKraja.ToString(), 
                PocetnaStanica, KrajnjaStanica);
        }

    }
}
