﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class Upravlja
    {
        public virtual UpravljaId Id { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }

        public Upravlja()
        {
            Id = new UpravljaId();
        }
    }
}
