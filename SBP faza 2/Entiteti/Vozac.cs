﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class Vozac: Zaposleni
    {
        
        public virtual int BrojDozvole { get; set; }
        public virtual String Kategorija { get; set; }

        public virtual IList<LicnoVozilo> LicnaVozila { get; set; }
        public virtual IList<Voznja> Voznje { get; set; }
        public virtual IList<VoziloFirme> VozilaFirme { get; set; }
        public virtual IList<Upravlja> UpravljaVozilaFirme { get; set; }

        public Vozac()
        {
            LicnaVozila = new List<LicnoVozilo>();
            Voznje = new List<Voznja>();
            VozilaFirme = new List<VoziloFirme>();
            UpravljaVozilaFirme = new List<Upravlja>();
        }

        public override string ToString()
        {
            return String.Format("Broj vozaca: {0}, Ime: {1} {2} {3}, Jmbg: {4}, Adresa: {5}, Telefon: {6}, Broj dozvole: {7}, Kategorija: {8}\n",
                IdZaposlenog.ToString(), LicnoIme, SrednjeSlovo, Prezime, JMBG.ToString(), Adresa, Telefon.ToString(), BrojDozvole.ToString(), Kategorija);
        }
        
    }
}
