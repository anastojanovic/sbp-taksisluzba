﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class Musterija
    { 
        public virtual int IdMusterije { get; protected set; }
        public virtual String Adresa { get; set; }
        public virtual int BrojVoznji { get; protected set; }

        public virtual IList<Voznja> Voznje { get; set; }
        public virtual IList<TelefonMusterije> Telefoni { get; set; }

        public Musterija()
        {
            Voznje = new List<Voznja>();
            Telefoni = new List<TelefonMusterije>();
        }

        public override string ToString()
        {
            return String.Format("Id: {0}, Adresa: {1}, Broj Voznji: {2}\n", IdMusterije.ToString(), Adresa, BrojVoznji.ToString());
        }

    }
}
