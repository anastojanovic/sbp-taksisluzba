﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class VoziloFirme 
    { 
        public virtual int IdVozila { get; protected set; }
        public virtual int Godiste { get; set; }
        public virtual String Marka { get; set; }
        public virtual String Tip { get; set; }
        public virtual String RegistarskaOznaka { get; set; }
        public virtual DateTime IstekRegistracije { get; set; }
        public virtual IList<Vozac> Vozaci { get; set; }
        public virtual IList<Upravlja> UpravljaVozac { get; set; }

        public VoziloFirme()
        {
            Vozaci = new List<Vozac>();
            UpravljaVozac = new List<Upravlja>();
        }

        public override string ToString()
        {
            return String.Format("Id: {0}, Godiste: {1}, Marka: {2}, Tip: {3}, Registracija: {4}, Istek: {5}\n",
                IdVozila.ToString(), Godiste.ToString(), Marka.ToString(), Tip.ToString(), RegistarskaOznaka.ToString(), IstekRegistracije.ToString());
        }

    }
}
