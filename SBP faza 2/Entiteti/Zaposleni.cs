﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public abstract class Zaposleni
    {
        public virtual int IdZaposlenog { get; protected set; }

        public virtual String LicnoIme { get; set; }
        public virtual String SrednjeSlovo { get; set; }
        public virtual String Prezime { get; set; }
        public virtual long JMBG { get; set; }
        public virtual String Adresa { get; set; }
        public virtual int Telefon { get; set; }

    }
}
