﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class TelefonMusterije 
    {
        public virtual int Telefon { get; set; }
        public virtual Musterija Musterija { get; set; }

        public override bool Equals(object obj)
        {
            if (Object.ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != typeof(TelefonMusterije))
                return false;

            TelefonMusterije recievedObject = (TelefonMusterije)obj;

            if ((Telefon == recievedObject.Telefon) &&
                (Musterija.IdMusterije == recievedObject.Musterija.IdMusterije))
            {
                return true;
            }

            return false;

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Telefon.ToString() + "\n";
        }
    }

}

