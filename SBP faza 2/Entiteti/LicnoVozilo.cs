﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class LicnoVozilo
    {
        //   public virtual LicnoVoziloId IdVozila { get; set; }
        public virtual int IdVozila { get; set; }
        public virtual String Marka { get; set; }
        public virtual String Tip { get; set; }
        public virtual String Boja { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }
        public virtual Vozac Vozac { get; set; }

        public LicnoVozilo()
        {
            Vozac = new Vozac();
        }

        public override string ToString()
        {
            return String.Format("Licno vozilo broj: {0}, Marka: {1}, Tip: {2}, Boja: {3}, Datum Od: {4}, Datum Do: {5}\n",
                IdVozila.ToString(), Marka, Tip, Boja, DatumOd.ToString(), DatumDo.ToString());
        }
    }
}

