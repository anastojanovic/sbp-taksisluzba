﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBP_faza_2.Entiteti
{
    public class Administrator: Zaposleni
    {
        public virtual String StrucnaSprema { get; set; }

        public virtual IList<Voznja> Voznje { get; set; }

        public Administrator()
        {
            Voznje = new List<Voznja>();
        }

        public override string ToString()
        {
            return String.Format("Administrator broj: {0}, Ime: {1} {2} {3}, JMBG: {4}, Adresa: {5}, Telefon: {6}, Strucna sprema: {7}\n",
                IdZaposlenog.ToString(), LicnoIme, SrednjeSlovo, Prezime, JMBG.ToString(), Adresa, Telefon.ToString(), StrucnaSprema);
        }

    }
}
