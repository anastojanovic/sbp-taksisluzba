﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace SBP_faza_2.Mapiranja
{
    public class UpravljaMapiranja: ClassMap<SBP_faza_2.Entiteti.Upravlja>
    {
        public UpravljaMapiranja()
        {
            Table("UPRAVLJA");

            CompositeId(x => x.Id)
                .KeyReference(x => x.UpravljaVozac, "ID_ZAPOSLENOG")
                .KeyReference(x => x.UpravljaVoziloFirme, "ID_VOZILA");

            Map(x => x.DatumOd).Column("DATUM_OD");
            Map(x => x.DatumDo).Column("DATUM_DO");

        }


    }

}
