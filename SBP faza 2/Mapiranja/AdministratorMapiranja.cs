﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace SBP_faza_2.Mapiranja
{
    public class AdministratorMapiranja: SubclassMap<SBP_faza_2.Entiteti.Administrator>
    {
        public AdministratorMapiranja()
        {
            Table("ADMINISTRATOR");

            Abstract();
            //Id(x => x.IdZaposlenog, "ID_ZAPOSLENOG").GeneratedBy.TriggerIdentity();

            Map(x => x.StrucnaSprema, "STRUCNA_SPREMA");


            HasMany(x => x.Voznje).KeyColumn("ID_ADMINISTRATORA").Cascade.All();
        }
    }
}
