﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace SBP_faza_2.Mapiranja
{
    class ZaposleniMapiranja: ClassMap<SBP_faza_2.Entiteti.Zaposleni>
    {
        public ZaposleniMapiranja()
        {
            UseUnionSubclassForInheritanceMapping();

            Id(x => x.IdZaposlenog, "ID_ZAPOSLENOG").GeneratedBy.TriggerIdentity();

            Map(x => x.LicnoIme, "LICNO_IME");
            Map(x => x.SrednjeSlovo, "SREDNJE_SLOVO");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.JMBG, "JMBG");
            Map(x => x.Adresa, "ADRESA");
            Map(x => x.Telefon, "TELEFON");

        }

    }
}
