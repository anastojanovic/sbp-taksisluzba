﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace SBP_faza_2.Mapiranja
{
    public class TelefonMusterijeMapiranja : ClassMap<SBP_faza_2.Entiteti.TelefonMusterije>
    {
        public TelefonMusterijeMapiranja()

        {
            Table("TELEFON_MUSTERIJE");
            CompositeId()
                .KeyProperty(x => x.Telefon,"TELEFON_MUSTERIJE")
                .KeyReference(x => x.Musterija,"ID_MUSTERIJE");


            // References(x => x.Musterija).Column("ID_MUSTERIJE").LazyLoad();
        }
    }
        
}
