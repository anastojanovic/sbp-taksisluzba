﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace SBP_faza_2.Mapiranja 
{
    public class VozacMapiranja : SubclassMap<SBP_faza_2.Entiteti.Vozac>
    {
        public VozacMapiranja()
        {
            Table("VOZAC");

            Abstract();
            Map(x => x.BrojDozvole, "BROJ_DOZVOLE");
            Map(x => x.Kategorija, "KATEGORIJA");


           HasMany(x => x.Voznje).KeyColumn("ID_VOZACA").Cascade.All();
           HasMany(x => x.LicnaVozila).KeyColumn("ID_ZAPOSLENOG").Cascade.All();
           HasMany(x => x.UpravljaVozilaFirme).KeyColumn("ID_ZAPOSLENOG").LazyLoad().Cascade.SaveUpdate().Inverse();

            HasManyToMany(x => x.VozilaFirme)
                  .Table("UPRAVLJA")
                  .ParentKeyColumn("ID_ZAPOSLENOG")
                  .ChildKeyColumn("ID_VOZILA").Cascade.SaveUpdate();

        
        }

    }
}
