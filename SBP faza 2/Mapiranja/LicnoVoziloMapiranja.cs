﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping; 
namespace SBP_faza_2.Mapiranja
{
    public class LicnoVoziloMapiranja : ClassMap<SBP_faza_2.Entiteti.LicnoVozilo>
    {
        public LicnoVoziloMapiranja()
        {
            Table("LICNO_VOZILO");
            Id(x => x.IdVozila, "ID_VOZILA").GeneratedBy.TriggerIdentity();
      //      CompositeId(x=>x.IdVozila).KeyProperty(x => x.IdVozila, "ID_VOZILA").KeyReference(x => x.Vozac, "ID_ZAPOSLENOG");

            Map(x => x.Marka, "MARKA");
            Map(x => x.Tip, "TIP");
            Map(x => x.Boja, "BOJA");
            Map(x => x.DatumOd, "DATUM_OD");
            Map(x => x.DatumDo, "DATUM_DO");

            References(x => x.Vozac).Column("ID_ZAPOSLENOG").LazyLoad();

        }
        
    }
}
