﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace SBP_faza_2.Mapiranja
{
    public class MusterijaMapiranja: ClassMap<SBP_faza_2.Entiteti.Musterija>
    {
        public MusterijaMapiranja()
        {
            Table("MUSTERIJA");

            Id(x => x.IdMusterije, "ID_MUSTERIJE").GeneratedBy.TriggerIdentity();

            Map(x => x.Adresa, "ADRESA");
            Map(x => x.BrojVoznji, "BROJ_VOZNJI");

            HasMany(x => x.Voznje).KeyColumn("ID_MUSTERIJE").Cascade.All();
            HasMany(x => x.Telefoni).KeyColumn("ID_MUSTERIJE").Inverse().Cascade.All();



        }
    }
}
