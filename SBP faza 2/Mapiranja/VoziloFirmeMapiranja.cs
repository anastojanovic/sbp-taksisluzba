﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace SBP_faza_2.Mapiranja
{
    public class VoziloFirmeMapiranja: ClassMap<SBP_faza_2.Entiteti.VoziloFirme>
    {
        public VoziloFirmeMapiranja()
        {
            Table("VOZILO_FIRME");
            Id(x => x.IdVozila, "ID_VOZILA").GeneratedBy.TriggerIdentity();

            Map(x => x.Marka, "MARKA");
            Map(x => x.Tip, "TIP");
            Map(x => x.Godiste, "GODISTE");
            Map(x => x.RegistarskaOznaka, "REGISTARSKA_OZNAKA");
            Map(x => x.IstekRegistracije, "ISTEK_REGISTRACIJE");

         HasMany(x => x.UpravljaVozac).KeyColumn("ID_VOZILA").LazyLoad().Cascade.All().Inverse();
            HasManyToMany(x => x.Vozaci)
                .Table("UPRAVLJA")
                .ParentKeyColumn("ID_VOZILA")
                .ChildKeyColumn("ID_ZAPOSLENOG")
                .Inverse()
                .Cascade.SaveUpdate();
         
            
        }
    }
}
