﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace SBP_faza_2.Mapiranja
{
    public class VoznjaMapiranja : ClassMap<SBP_faza_2.Entiteti.Voznja>
    {
        public VoznjaMapiranja()
        {
            Table("VOZNJA");
            Id(x => x.IdVoznje, "ID_VOZNJE").GeneratedBy.TriggerIdentity();


            Map(x => x.VremePrimanjaPoziva, "VREME_PRIMANJA_POZIVA");
            Map(x => x.PozivniTelefon, "POZIVNI_TELEFON");
            Map(x => x.VremePocetka, "VREME_POCETKA");
            Map(x => x.VremeKraja, "VREME_KRAJA");
            Map(x => x.PocetnaStanica, "POCETNA_STANICA");
            Map(x => x.KrajnjaStanica, "KRAJNJA_STANICA");

            References(x => x.Administrator).Column("ID_ADMINISTRATORA").LazyLoad();
            References(x => x.Vozac).Column("ID_VOZACA").LazyLoad();
            References(x => x.Musterija).Column("ID_MUSTERIJE").LazyLoad();

        }
    }
}
