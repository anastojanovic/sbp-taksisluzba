﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOs;
using SBP_faza_2.DTOManagers;

namespace WebApplication.Controllers
{
    public class VozacController : ApiController
    {
        // GET: api/Vozac
        public IEnumerable<VozacBasicDTO> Get()
        {
            VozacDTOManager manager = new VozacDTOManager();
            return (IEnumerable<VozacBasicDTO>)manager.ReadAll();
        }

        // GET: api/Vozac/5
        public VozacSuperLongDTO Get(int id)
        {
            VozacDTOManager manager = new VozacDTOManager();
            return manager.Read(id);
        }

        // POST: api/Vozac
        public void Post([FromBody]VozacLongDTO value)   //http://localhost:54301/Properties/
        {
            VozacDTOManager manager = new VozacDTOManager();
            manager.Create(value);
        }

        // PUT: api/Vozac/5
        public void Put(int id, [FromBody]VozacLongDTO value)
        {
            VozacDTOManager manager = new VozacDTOManager();
            value.Id = id;
            manager.Update(value);
        }

        // DELETE: api/Vozac/5
        public void Delete(int id)
        {
            VozacDTOManager manager = new VozacDTOManager();
            manager.Delete(id);
        }
    }
}
