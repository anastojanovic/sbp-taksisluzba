﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOs;
using SBP_faza_2.DTOManagers;

namespace WebApplication.Controllers
{
    public class AdministratorController : ApiController
    {
        // GET: api/Administrator
        public IEnumerable<AdministratorBasicDTO> Get()
        {
            AdministratorDTOManager manager = new AdministratorDTOManager();
            return(IEnumerable<AdministratorBasicDTO>)manager.ReadAll();
        }

        // GET: api/Administrator/5
        public AdministratorSuperLongDTO Get(int id)
        {
            AdministratorDTOManager manager = new AdministratorDTOManager();
            return manager.Read(id);
        }

        // POST: api/Administrator
        public void Post([FromBody]AdministratorLongDTO value)
        {
            AdministratorDTOManager manager = new AdministratorDTOManager();
            manager.Create(value);
        }

        // PUT: api/Administrator/5
        public void Put(int id, [FromBody]AdministratorLongDTO value)
        {
            AdministratorDTOManager manager = new AdministratorDTOManager();
            value.Id = id;
            manager.Update(value);
        }

        // DELETE: api/Administrator/5
        public void Delete(int id)
        {
            AdministratorDTOManager manager = new AdministratorDTOManager();
            manager.Delete(id);
        }
    }
}
