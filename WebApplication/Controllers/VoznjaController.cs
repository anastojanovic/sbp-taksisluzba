﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;

namespace WebApplication.Controllers
{
    public class VoznjaController : ApiController
    {
        // GET: api/Voznja
        public IEnumerable<VoznjaBasicDTO> Get()
        {
            VoznjaDTOManager manager = new VoznjaDTOManager();
            return (IEnumerable<VoznjaBasicDTO>)manager.ReadAll();
           
        }

        // GET: api/Voznja/5
        public VoznjaSuperLongDTO Get(int id)
        {
            VoznjaDTOManager manager = new VoznjaDTOManager();
            return (VoznjaSuperLongDTO)manager.Read(id);
        }
        

        // POST: api/Voznja
        public void Post([FromBody]VoznjaLongDTO value)
        {
            VoznjaDTOManager manager = new VoznjaDTOManager();
            manager.Create(value);
        }

        // PUT: api/Voznja/5
        public void Put(int id, [FromBody]VoznjaLongDTO value)
        {

            VoznjaDTOManager manager = new VoznjaDTOManager();
            value.Id = id;
            manager.Update(value);
        }

        // DELETE: api/Voznja/5
        public void Delete(int id)
        {
            VoznjaDTOManager manager = new VoznjaDTOManager();
            manager.Delete(id);
        }
    }
}
