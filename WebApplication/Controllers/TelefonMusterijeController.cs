﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;

namespace WebApplication.Controllers
{
    [RoutePrefix("api/TelefonMusterije")]
    public class TelefonMusterijeController : ApiController
    {
        // GET: api/TelefonMusterije/Musterija/5    //Uzima sve telefone te musterije
        [HttpGet]
        [Route("Musterija/{id:int}")]
        public IEnumerable<TelefonDTO> GetTelefoniMusterije(int id)
        {
            TelefonMusterijeDTOManager manager = new TelefonMusterijeDTOManager();
            return (IEnumerable<TelefonDTO>)manager.ReadAll(id);
        }
        
        [HttpPost]
        [Route("")]
        public void Post([FromBody]TelefonDTO value)
        {
            TelefonMusterijeDTOManager manager = new TelefonMusterijeDTOManager();
            manager.Create(value);
        }
        

        // DELETE: api/TelefonMusterije/Musterija/5/Telefon/5
        [HttpDelete]
        [Route("Musterija/{idMusterije:int}/Telefon/{idTelefona:int}")]         //brise konkretan telefon konkretne musterije
        public void Delete(int idMusterije, int idTelefona)
        {
            TelefonMusterijeDTOManager manager = new TelefonMusterijeDTOManager();
            TelefonDTO dto = new TelefonDTO(idMusterije, idTelefona);
            manager.Delete(dto);
        }
    }
}
