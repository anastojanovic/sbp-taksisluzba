﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;

namespace WebApplication.Controllers
{
    public class MusterijaController : ApiController
    {
        // GET: api/Musterija
        public IEnumerable<MusterijaBasicDTO> Get()
        {
            MusterijaDTOManager manager = new MusterijaDTOManager();
            return (IEnumerable<MusterijaBasicDTO>)manager.ReadAll();
        }
    

        // GET: api/Musterija/5
        public MusterijaSuperLongDTO Get(int id)
        {
            MusterijaDTOManager manager = new MusterijaDTOManager();
            return manager.Read(id);
        }

        // POST: api/Musterija
        public void Post([FromBody]MusterijaBasicDTO value)
        {
            MusterijaDTOManager manager = new MusterijaDTOManager();
            manager.Create(value);
        }

        // PUT: api/Musterija/5
        public void Put(int id, [FromBody]MusterijaBasicDTO value)
        {
            MusterijaDTOManager manager = new MusterijaDTOManager();
            value.Id = id;
            manager.Update(value);
        }

        // DELETE: api/Musterija/5
        public void Delete(int id)
        {
            MusterijaDTOManager manager = new MusterijaDTOManager();
            manager.Delete(id);
        }
    }
}
