﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;
using System.Net.Http.Formatting;

namespace WebApplication.Controllers
{
    [RoutePrefix("api/LicnoVozilo")]
    public class LicnoVoziloController : ApiController
    {

        // GET: api/LicnoVozilo/5
        [HttpGet]
        public LicnoVoziloSuperLongDTO Get(int id)
        {
            LicnoVoziloDTOManager manager = new LicnoVoziloDTOManager();
            return manager.Read(id);
        }

        //GET: api/LicnoVozilo/Vozac/5
        [HttpGet]
        [ActionName("Vozac")]
        [Route("Vozac/{id:int}")]
        public IEnumerable<LicnoVoziloShortDTO> GetByVozacId(int id)
        {
            LicnoVoziloDTOManager manager = new LicnoVoziloDTOManager();
            return (IEnumerable<LicnoVoziloShortDTO>)manager.ReadAll(id);
        }

        // POST: api/LicnoVozilo
        public void Post([FromBody]LicnoVoziloLongDTO value)
        {
            LicnoVoziloDTOManager manager = new LicnoVoziloDTOManager();
            manager.Create(value);
        }

        // PUT: api/LicnoVozilo/5
        public void Put(int id, [FromBody]LicnoVoziloLongDTO value)
        {
            LicnoVoziloDTOManager manager = new LicnoVoziloDTOManager();
            value.Id = id;
            manager.Update(value);

        }

        // DELETE: api/LicnoVozilo/5
        public void Delete(int id)
        {
            LicnoVoziloDTOManager manager = new LicnoVoziloDTOManager();
            manager.Delete(id);
        }
    }
}
