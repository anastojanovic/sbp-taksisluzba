﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;

namespace WebApplication.Controllers
{
    public class VoziloFirmeController : ApiController
    {
        // GET: api/VoziloFirme
        public IEnumerable<VoziloFirmeBasicDTO> Get()
        {
            VoziloFirmeDTOManager manager = new VoziloFirmeDTOManager();
            return (IEnumerable<VoziloFirmeBasicDTO>)manager.ReadAll();
        }

        // GET: api/VoziloFirme/5
        public VoziloFirmeSuperLongDTO Get(int id)
        {
            
            VoziloFirmeDTOManager manager = new VoziloFirmeDTOManager();
            return manager.Read(id);
        }

        // POST: api/VoziloFirme
        public void Post([FromBody]VoziloFirmeLongDTO value)
        {
            VoziloFirmeDTOManager manager = new VoziloFirmeDTOManager();
            manager.Create(value);
        }

        // PUT: api/VoziloFirme/5
        public void Put(int id, [FromBody]VoziloFirmeLongDTO value)
        {
            VoziloFirmeDTOManager manager = new VoziloFirmeDTOManager();
            value.Id = id;
            manager.Update(value);
        }

        // DELETE: api/VoziloFirme/5
        public void Delete(int id)
        {
            VoziloFirmeDTOManager manager = new VoziloFirmeDTOManager();
            manager.Delete(id);
        }
    }
}
