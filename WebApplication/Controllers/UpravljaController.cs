﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBP_faza_2.DTOManagers;
using SBP_faza_2.DTOs;

namespace WebApplication.Controllers
{
    [RoutePrefix("api/Upravlja")]
    public class UpravljaController : ApiController
    {
        // GET: api/Upravlja
        [HttpGet]
        [Route("")]
        public IEnumerable<UpravljaDTO> Get()
        {
            UpravljaDTOManager manager = new UpravljaDTOManager();
            return manager.ReadAll();
        }

        // GET: api/Upravlja/Vozac/5/Vozilo/5
        [HttpGet]
        [Route("Vozac/{idVozaca:int}/Vozilo/{idVozila:int}")]
        public UpravljaLongDTO Get(int idVozaca, int idVozila)
        {
            UpravljaDTOManager manager = new UpravljaDTOManager();
            return manager.Read(idVozila, idVozaca);
        }

        // POST: api/Upravlja
        [HttpPost]
        [Route("")]
        public void Post([FromBody]UpravljaDTO dto)
        {
            UpravljaDTOManager manager = new UpravljaDTOManager();
            manager.Create(dto);
        }

        // PUT: api/Upravlja/
        [HttpPut]
        [Route("")]
        public void Put([FromBody]UpravljaDTO value)
        {
            UpravljaDTOManager manager = new UpravljaDTOManager();
            manager.Update(value);
        }

        // DELETE: api/Upravlja/Vozac/5/Vozilo/5
        [HttpDelete]
        [Route("Vozac/{idVozaca:int}/Vozilo/{idVozila:int}")]
        public void Delete(int idVozaca, int idVozila)
        {
            UpravljaDTOManager manager = new UpravljaDTOManager();
            manager.Delete(idVozaca, idVozila);
        }
    }
}
